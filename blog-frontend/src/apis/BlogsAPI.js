import axios from 'axios'

export default class BlogsAPI {
  async fetchPublishedPosts() {
    return axios.get('/api/post')
  }

  async fetchPostBySlug(slug) {
    return axios.get(`/api/post/${slug}`)
  }

  async fetchAllPosts() {
    return axios.get('/api/admin/post')
  }

  async fetchPostById(id) {
    return axios.get(`/api/admin/post/${id}`)
  }

  async savePost(post) {
    return axios.post('/api/admin/post', post)
  }

  async deletePost(id) {
    return axios.delete(`/api/admin/post/${id}`)
  }

  async updatePost(post) {
    return axios.put('/api/admin/post', post)
  }

  async fetchPublishedPages() {
    return axios.get('/api/page')
  }

  async fetchPageBySlug(slug) {
    return axios.get(`/api/page/${slug}`)
  }

  async fetchAllPages() {
    return axios.get('/api/admin/page')
  }

  async fetchPageById(id) {
    return axios.get(`/api/admin/page/${id}`)
  }

  async savePage(page) {
    return axios.post('/api/admin/page', page)
  }

  async deletePage(id) {
    return axios.delete(`/api/admin/page/${id}`)
  }

  async updatePage(page) {
    return axios.put('/api/admin/page', page)
  }

  async login(payload) {
    return axios.post('/api/auth/jwt', payload)
  }
}