import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit'
import {BlogsAPI} from '../apis'

const api = new BlogsAPI()

export const fetchPages = createAsyncThunk(
  'pages/fetchAll',
  async () => (await api.fetchPublishedPages()).data
)

export const fetchPageBySlug = createAsyncThunk(
  'pages/fetchBySlug',
  async (slug, _) => (await api.fetchPageBySlug(slug)).data
)

const pagesAdapter = createEntityAdapter({
  selectId: page => page.slug
})
const initialState = pagesAdapter.getInitialState()

const pageSlice = createSlice({
  name: 'pages',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchPages.pending, state => {
      state.isLoading = true
    })
    builder.addCase(fetchPages.fulfilled, (state, action) => {
      state.isLoading = false
      pagesAdapter.setAll(state, action.payload)
    })
    builder.addCase(fetchPages.rejected, (state, action) => {
      console.log(action.error)
      state.isLoading = false
    })
    builder.addCase(fetchPageBySlug.pending, (state => {
      state.isLoading = true
    }))
    builder.addCase(fetchPageBySlug.fulfilled, ((state, action) => {
      state.isLoading = false
      pagesAdapter.upsertOne(state, action.payload)
    }))
    builder.addCase(fetchPageBySlug.rejected, (state, action) => {
      console.log(action.error)
      state.isLoading = false
    })
  }
})

export const {
  selectById: selectPageBySlug,
  selectIds: selectPageIds,
  selectEntities: selectPageEntities,
  selectAll: selectAllPages,
  selectTotal: selectTotalPages
} = pagesAdapter.getSelectors(state => state.pages)

export default pageSlice.reducer
