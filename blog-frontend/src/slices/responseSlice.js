import {createSlice} from "@reduxjs/toolkit";

const initialState = {
  message: '',
  theme: '',
  isLoading: false
}

const responseSlice = createSlice({
  name: 'response',
  initialState,
  extraReducers: builder => {

  }
})

const handleFulfilled = (state, action) => {
  state.message = action.payload.message
  state.theme = 'success'
  state.isLoading = false;
}

const handleRejected = (state, action) => {
  state.message = action.payload? action.payload.message: action.error.message
  state.theme = 'danger'
  state.isLoading = false
}

const handlePending = (state) => {
  state.message = ''
  state.theme = 'dark'
  state.isLoading = true
}

export default responseSlice.reducer
