import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit'
import {BlogsAPI} from '../apis'

const api = new BlogsAPI()

export const fetchPosts = createAsyncThunk(
  'posts/fetchAll',
  async () => (await api.fetchPublishedPosts()).data
)

export const fetchPostBySlug = createAsyncThunk(
  'posts/fetchBySlug',
  async (slug, _) => (await api.fetchPostBySlug(slug)).data
)

const postsAdapter = createEntityAdapter({
  selectId: post => post.slug
})

const initialState = postsAdapter.getInitialState()

const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchPosts.pending, state => {
      state.isLoading = true
    })
    builder.addCase(fetchPosts.fulfilled, (state, action) => {
      state.isLoading = false
      postsAdapter.setAll(state, action.payload)
    })
    builder.addCase(fetchPosts.rejected, (state, action) => {
      console.log(action.error)
      state.isLoading = false
    })
    builder.addCase(fetchPostBySlug.pending, (state => {
      state.isLoading = true
    }))
    builder.addCase(fetchPostBySlug.fulfilled, ((state, action) => {
      state.isLoading = false
      postsAdapter.upsertOne(state, action.payload)
    }))
    builder.addCase(fetchPostBySlug.rejected, (state, action) => {
      console.log(action.error)
      state.isLoading = false
    })
  }
})

export const {
  selectById: selectPostBySlug,
  selectIds: selectPostIds,
  selectEntities: selectPostEntities,
  selectAll: selectAllPosts,
  selectTotal: selectTotalPosts
} = postsAdapter.getSelectors(state => state.posts)

export default postsSlice.reducer
