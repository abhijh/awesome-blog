import React from "react";
import PropTypes from "prop-types";
import {Container} from "react-bootstrap";
import BlogNavbar from "../components/layout/Navbar/BlogNavbar";

const BlogLayout = ({children, noNavbar}) => (
  <div>
    {!noNavbar && <BlogNavbar/>}
    <Container fluid>
      {children}
    </Container>
  </div>
);

BlogLayout.propTypes = {
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool,
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool
};

BlogLayout.defaultProps = {
  noNavbar: false,
  noFooter: false
};

export default BlogLayout;
