import React from 'react'
// Layout Types
import {BlogLayout} from "./layouts";
// Route Views
import Posts from "./views/Posts";
import BlogPost from "./views/Post";
import {Redirect} from "react-router-dom";
import Page from "./views/Page";

export default [
  {
    path: "/",
    layout: BlogLayout,
    component: Posts,
    exact: true
  },
  {
    path: "/post/:slug",
    layout: BlogLayout,
    component: BlogPost,
    exact: true
  },
  {
    path: "/page/:slug",
    layout: BlogLayout,
    component: Page,
    exact: true
  },
  {
    path: "*",
    layout: BlogLayout,
    component: () => <Redirect to='/' />
  }
];
