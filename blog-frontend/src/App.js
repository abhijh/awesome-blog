import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import routes from "./routes";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/font-awesome.min.css";
import "./assets/css/classy-nav.css"
import "./assets/css/nice-select.css"
import "./assets/css/themify-icons.css"
import "./assets/css/animate.css"
import "./assets/css/magnific-popup.css"
import "./assets/css/styles.css"
import './utils/interceptors'
// import withTracker from "./withTracker";

export default () => (
  <Router basename={process.env.REACT_APP_BASENAME || ""}>
    <Switch>
      {routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={props => {
              return (
                <route.layout
                  {...props}
                  noNavbar={route.noNavbar}
                  noFooter={route.noFooter}>
                  <route.component {...props} />
                </route.layout>
              );
            }}
          />
        );
      })}
    </Switch>
  </Router>
);
