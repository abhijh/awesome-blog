import { combineReducers } from 'redux';
import postsReducer from '../slices/postSice'
import pageReducer from '../slices/pageSice'
import authReducer from '../slices/authSice'
import responseReducer from '../slices/responseSlice'

export default combineReducers({
  response: responseReducer,
  posts: postsReducer,
  pages: pageReducer,
  auth: authReducer
})
