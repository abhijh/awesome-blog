/* eslint jsx-a11y/anchor-is-valid: 0 */
import React, {useEffect} from "react"
import {Link, withRouter} from "react-router-dom"
import {useDispatch, useSelector} from 'react-redux'
import {fetchPostBySlug, selectPostBySlug} from "../slices/postSice"
import ReactMarkdown from "react-markdown/with-html"
import 'react-markdown-editor-lite/lib/index.css'
import {CodeBlock, Image} from "../components/common/renderers";
import {DiscussionEmbed} from 'disqus-react';
import Moment from "react-moment";

const Post = ({history, match, location}) => {
  const slug = match.params.slug
  const dispatch = useDispatch()
  const post = useSelector(
    (state) => selectPostBySlug(state, slug))
  useEffect(() => {
    if (!post) {
      dispatch(fetchPostBySlug(slug))
        .then(response => {
          response.type === fetchPostBySlug.fulfilled.toString() &&
          history.push('/')
        })
    }
  }, [slug])
  return (
    <div>
      {post && <div>
        {/*<div className="vizew-pager-next">*/}
        {/*  <p>SHARE</p>*/}
        {/*  <div className="pager-article">*/}
        {/*    <p>Share This Post</p>*/}
        {/*    <div className="single-widget share-post-widget mb-50">*/}
        {/*      <FacebookShareButton*/}
        {/*        url={window.location.href}*/}
        {/*        hashtag={post.tags.map(_ => '#'+_.name).join()}*/}
        {/*        quote={post.title}>*/}
        {/*        /!*<FacebookIcon size={32} round={true} />*!/*/}
        {/*        <a className="facebook"><i className="fa fa-facebook" aria-hidden="true"></i> Facebook</a>*/}
        {/*      </FacebookShareButton>*/}
        {/*      <a href="#" className="twitter"><i className="fa fa-twitter" aria-hidden="true"></i> Twitter</a>*/}
        {/*      <a href="#" className="google"><i className="fa fa-google" aria-hidden="true"></i> Google+</a>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*</div>*/}
        {/*<div className="vizew-breadcrumb">*/}
        {/*  <div className="container">*/}
        {/*    <div className="row">*/}
        {/*      <div className="col-12">*/}
        {/*        <nav aria-label="breadcrumb">*/}
        {/*          <ol className="breadcrumb">*/}
        {/*            <li className="breadcrumb-item"><Link to="/"><i className="fa fa-home" aria-hidden="true"></i> Home</Link>*/}
        {/*            </li>*/}
        {/*            <li className="breadcrumb-item"><Link to="/">Posts</Link></li>*/}
        {/*            <li className="breadcrumb-item active" aria-current="page">{post.title}</li>*/}
        {/*          </ol>*/}
        {/*        </nav>*/}
        {/*      </div>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*</div>*/}
        <section className="post-details-area mb-80">
          <div className="container">
            {/*<div className="row">*/}
            {/*  <div className="col-12">*/}
            {/*    <div className="post-details-thumb mb-50">*/}
            {/*      <img src={post.backgroundImage} alt=""/>*/}
            {/*    </div>*/}
            {/*  </div>*/}
            {/*</div>*/}
            <div className="row justify-content-center">
              <div className="col-12 col-md-10 col-lg-9 col-xl-8">
                <div className="vizew-breadcrumb">
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"
                                                                      aria-hidden="true"/> Home</Link>
                      </li>
                      <li className="breadcrumb-item"><Link to="/">Posts</Link></li>
                      <li className="breadcrumb-item active" aria-current="page">{post.title}</li>
                    </ol>
                  </nav>
                </div>
                <div className="post-details-content">
                  <div className="blog-content">
                    <div className="post-content mt-0">
                      <a href="#" className={`post-cata cata-sm cata-${post.categoryTheme}`}>{post.category}</a>
                      <h1 className="mb-2">{post.title}</h1>
                      <div className="d-flex justify-content-between mb-30">
                        <div className="post-meta d-flex align-items-center">
                          <a href="#" className="post-author">By {post.author.name}</a>
                          <i className="fa fa-circle" aria-hidden="true"/>
                          <a href="#" className="post-date">
                            <Moment format="MMMM Do YYYY, h:mm a" date={post.createdDate}/>
                          </a>
                        </div>
                        <div className="post-meta d-flex">
                          <a href="#"><i className="fa fa-comments-o" aria-hidden="true"/> 32</a>
                          <a href="#"><i className="fa fa-eye" aria-hidden="true"/> 42</a>
                          <a href="#"><i className="fa fa-thumbs-o-up" aria-hidden="true"/> 7</a>
                        </div>
                      </div>
                    </div>
                    <div className="blog-text">
                      <ReactMarkdown
                        source={post.body}
                        escapeHtml={false}
                        renderers={{
                          code: CodeBlock,
                          image: Image
                        }}/>
                    </div>
                    {post.tags.length !== 0 && <div className="post-tags mt-30">
                      <ul>
                        {post.tags.map((tag, idx) => <li key={idx}><Link to="/">{tag.name}</Link></li>)}
                      </ul>
                    </div>}

                    {/*<div className="related-post-area mt-5">*/}

                    {/*  <div className="section-heading style-2">*/}
                    {/*    <h4>Related Post</h4>*/}
                    {/*    <div className="line"></div>*/}
                    {/*  </div>*/}
                    {/*  <div className="row">*/}

                    {/*    <div className="col-12 col-md-6">*/}
                    {/*      <div className="single-post-area mb-50">*/}

                    {/*        <div className="post-thumbnail">*/}
                    {/*          <img src="img/bg-img/11.jpg" alt=""/>*/}

                    {/*          <span className="video-duration">05.03</span>*/}
                    {/*        </div>*/}

                    {/*        <div className="post-content">*/}
                    {/*          <a href="#" className="post-cata cata-sm cata-success">Sports</a>*/}
                    {/*          <a href="single-post.html" className="post-title">Warner Bros. Developing ‘The accountant’ Sequel</a>*/}
                    {/*          <div className="post-meta d-flex">*/}
                    {/*            <a href="#"><i className="fa fa-comments-o" aria-hidden="true"></i> 22</a>*/}
                    {/*            <a href="#"><i className="fa fa-eye" aria-hidden="true"></i> 16</a>*/}
                    {/*            <a href="#"><i className="fa fa-thumbs-o-up" aria-hidden="true"></i> 15</a>*/}
                    {/*          </div>*/}
                    {/*        </div>*/}
                    {/*      </div>*/}
                    {/*    </div>*/}

                    {/*    <div className="col-12 col-md-6">*/}
                    {/*      <div className="single-post-area mb-50">*/}

                    {/*        <div className="post-thumbnail">*/}
                    {/*          <img src="img/bg-img/12.jpg" alt=""/>*/}

                    {/*          <span className="video-duration">05.03</span>*/}
                    {/*        </div>*/}

                    {/*        <div className="post-content">*/}
                    {/*          <a href="#" className="post-cata cata-sm cata-danger">Game</a>*/}
                    {/*          <a href="single-post.html" className="post-title">Searching for the 'angel' who held me on Westminste</a>*/}
                    {/*          <div className="post-meta d-flex">*/}
                    {/*            <a href="#"><i className="fa fa-comments-o" aria-hidden="true"></i> 28</a>*/}
                    {/*            <a href="#"><i className="fa fa-eye" aria-hidden="true"></i> 17</a>*/}
                    {/*            <a href="#"><i className="fa fa-thumbs-o-up" aria-hidden="true"></i> 22</a>*/}
                    {/*          </div>*/}
                    {/*        </div>*/}
                    {/*      </div>*/}
                    {/*    </div>*/}
                    {/*  </div>*/}
                    {/*</div>*/}
                    <div className="vizew-post-author d-flex align-items-center py-5">
                      <div className="post-author-thumb">
                        <img src={post.author.avatar} alt=""/>
                      </div>
                      <div className="post-author-desc pl-4">
                        <a href="#" className="author-name">{post.author.name}</a>
                        <p>I am a programmer, lover of technology and a vivid traveller.</p>
                        <div className="post-author-social-info">
                          <a href="#"><i className="fa fa-facebook"/></a>
                          <a href="#"><i className="fa fa-twitter"/></a>
                          <a href="#"><i className="fa fa-pinterest"/></a>
                          <a href="#"><i className="fa fa-linkedin"/></a>
                          <a href="#"><i className="fa fa-dribbble"/></a>
                        </div>
                      </div>
                    </div>
                    <div className="comment_area clearfix mb-50">
                      <div className="section-heading style-2">
                        <h4>Comment</h4>
                        <div className="line"/>
                      </div>
                      <DiscussionEmbed
                        shortname="abhinavjha"
                        config={
                          {
                            url: window.location.href,
                            identifier: String(post.id),
                            title: post.title,
                          }}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>}
    </div>
    // <Container fluid className="main-content-container px-4">
    //   {post && <div>
    //     <Row style={{
    //       fontFamily: '"Work Sans","Helvetica Neue",sans-serif !important',
    //       fontSize: '18px',
    //       lineHeight: 1.675,
    //       letterSpacing: '.3px',
    //       marginTop: '20px'
    //     }}>
    //       <Col sm={12} md={{ span: 6, offset: 3 }}>
    //         <h1>{post.title}</h1>
    //         <ReactMarkdown
    //           source={post.body}
    //           escapeHtml={false}
    //           renderers={{
    //             code: CodeBlock,
    //             image: Image
    //           }}/>
    //         <div className="comment_area clearfix mb-50">
    //           <div className="section-heading style-2">
    //             <h4>Comment</h4>
    //             <div className="line"></div>
    //           </div>
    //           <DiscussionEmbed
    //             shortname="abhinavjha"
    //             config={
    //               {
    //                 url: window.location.href,
    //                 identifier: String(post.id),
    //                 title: post.title,
    //               }
    //             }
    //           />
    //         </div>
    //       </Col>
    //     </Row>
    //   </div>}
    // </Container>
  );
}
export default withRouter(Post)
