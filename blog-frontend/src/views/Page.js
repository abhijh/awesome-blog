/* eslint jsx-a11y/anchor-is-valid: 0 */
import React, {useEffect} from "react"
import {withRouter} from "react-router-dom"
import {useDispatch, useSelector} from 'react-redux'

import {Col, Container, Row} from "react-bootstrap"
import {fetchPageBySlug, selectPageBySlug} from "../slices/pageSice"
import ReactMarkdown from "react-markdown/with-html"
import {CodeBlock, Image} from "../components/common/renderers";
import 'react-markdown-editor-lite/lib/index.css'

const Page = ({history, match}) => {
  const slug = match.params.slug
  const dispatch = useDispatch()
  const post = useSelector(
    (state) => selectPageBySlug(state, slug))
  useEffect(() => {
    if(!post) {
      dispatch(fetchPageBySlug(slug))
        .then(response => {
          response.type === fetchPageBySlug.rejected.toString() &&
            history.push('/')
        })
    }},[slug])

  return (
    <Container fluid className="main-content-container px-4">
      {post && <div>
        <Row style={{
          fontFamily: '"Work Sans","Helvetica Neue",sans-serif',
          fontSize: '18px',
          lineHeight: 1.675,
          letterSpacing: '.3px',
          marginTop: '20px'
        }}>
          <Col sm={12} md={{ span: 6, offset: 3 }}>
            <ReactMarkdown
              source={post.body}
              escapeHtml={false}
              renderers={{
                code: CodeBlock,
                image: Image
              }}/>
          </Col>
        </Row>
      </div>}
    </Container>
  );
}

export default withRouter(Page)
