/* eslint jsx-a11y/anchor-is-valid: 0 */
import React, {useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {fetchPosts, selectAllPosts} from "../slices/postSice";
import {Link, withRouter} from "react-router-dom";

const Posts = ({history}) => {
  const dispatch = useDispatch()
  const posts = useSelector(selectAllPosts)
  useEffect(() => {
    dispatch(fetchPosts())
  }, [])
  return (
    <section className="trending-posts-area">
      <div className="container mt-5">
        {/*<div className="row">*/}
        {/*  <div className="col-12">*/}
        {/*    <div className="section-heading">*/}
        {/*      <h4>Trending Posts</h4>*/}
        {/*      <div className="line"></div>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*</div>*/}
        <div className="row">
          {posts.map((post, idx) => (
            <div className="col-12 col-md-4" key={idx}>
              <div className="single-post-area mb-80">
                <div onClick={() => history.push(`/post/${post.slug}`)}
                     style={{
                       cursor: 'pointer',
                       backgroundImage: `url('${post.backgroundImage}')`
                     }} className="post-thumbnail card-post__thumbnail">
                  {/*<img src={post.backgroundImage} alt=""/>*/}
                    <span className="video-duration">05.03</span>
                </div>
                <div className="post-content">
                  <Link to="/" className={`post-cata cata-sm cata-${post['categoryTheme']}`}>{post.category}</Link>
                  <Link to={`/post/${post.slug}`} className="post-title">{post.title}</Link>
                  {/*<span className="text-muted">{post.createdDate}</span>*/}
                  <span className="card-text d-inline-block mb-3"
                    style={styles.description}
                    dangerouslySetInnerHTML={{__html: post.description.length > 225 ? `${post.description.substr(0, 222)}...` : post.description}}/>
                  {/*<div className="post-meta d-flex">*/}
                  {/*  <a href="#"><i className="fa fa-comments-o" aria-hidden="true"></i> 22</a>*/}
                  {/*  <a href="#"><i className="fa fa-eye" aria-hidden="true"></i> 16</a>*/}
                  {/*  <a href="#"><i className="fa fa-thumbs-o-up" aria-hidden="true"></i> 15</a>*/}
                  {/*</div>*/}
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
    // <Container fluid className="main-content-container px-4">
    //   {/* First Row of Posts */}
    //   <Row className="page-header py-4">
    //     {posts.map((post, idx) => (
    //       <Col lg="3" md="4" sm="12" className="mb-4" key={idx}>
    //         <Card className="card-post card-post--1">
    //           <div
    //             className="card-post__image"
    //             style={{backgroundImage: `url('${post.backgroundImage}')`}}>
    //             <Badge
    //               pill
    //               className={`card-post__category bg-${post['categoryTheme']}`}>
    //               {post.category}
    //             </Badge>
    //             <div className="card-post__author d-flex">
    //               <a
    //                 href="#"
    //                 className="card-post__author-avatar card-post__author-avatar--small"
    //                 style={{backgroundImage: `url('${post.author.avatar}')`}}>
    //                 Written by {" " + post.author.name}
    //               </a>
    //             </div>
    //           </div>
    //           <Card.Body style={styles.cardBody}>
    //             <h5 className="card-title">
    //               <Link to={`/post/${post.slug}`} className="text-fiord-blue">
    //                 {post.title}
    //               </Link>
    //             </h5>
    //             <p className="card-text d-inline-block mb-3"
    //                style={styles.description}
    //                dangerouslySetInnerHTML={{__html: post.description.length > 200 ? `${post.description.substr(0, 197)}...` : post.description}}/>
    //             <span className="text-muted">{post.date}</span>
    //           </Card.Body>
    //           {/*<CardFooter className="border-top d-flex">*/}
    //           {/*  <div className="my-auto ml-auto">*/}
    //           {/*    <Button size="sm" theme="white" onClick={() => history.push(`/${post.id}/${post.slug}`)}>*/}
    //           {/*      <i className="fas fa-long-arrow-alt-right mr-1"/> Continue to post*/}
    //           {/*    </Button>*/}
    //           {/*  </div>*/}
    //           {/*</CardFooter>*/}
    //         </Card>
    //       </Col>
    //     ))}
    //   </Row>
    //
    //   Second Row of Posts
    //   <Row>
    //     {posts.slice(0, 2).map((post, idx) => (
    //       <Col lg="6" sm="12" className="mb-4" key={idx}>
    //         <Card small className="card-post card-post--aside card-post--1">
    //           <div
    //             className="card-post__image"
    //             style={{backgroundImage: `url('${post.backgroundImage}')`}}
    //           >
    //             <Badge
    //               pill
    //               className={`card-post__category bg-${post['categoryTheme']}`}
    //             >
    //               {post.category}
    //             </Badge>
    //             <div className="card-post__author d-flex">
    //               <a
    //                 href="#"
    //                 className="card-post__author-avatar card-post__author-avatar--small"
    //                 style={{backgroundImage: `url('${post.author.avatar}')`}}
    //               >
    //                 Written by {" " + post.author.name}
    //               </a>
    //             </div>
    //           </div>
    //           <CardBody>
    //             <h5 className="card-title">
    //               <a className="text-fiord-blue" href="#">
    //                 {post.title}
    //               </a>
    //             </h5>
    //             <p className="card-text d-inline-block mb-3" dangerouslySetInnerHTML={{__html: post.description}}/>
    //             <span className="text-muted">{post.date}</span>
    //           </CardBody>
    //         </Card>
    //       </Col>
    //     ))}
    //   </Row>
    //
    //    Third Row of Posts
    //   <Row>
    //     {posts.map((post, idx) => (
    //       <Col lg="4" key={idx}>
    //         <Card small className="card-post mb-4">
    //           <CardBody>
    //             <h5 className="card-title">{post.title}</h5>
    //             <p className="card-text text-muted" dangerouslySetInnerHTML={{__html: post.description}}/>
    //           </CardBody>
    //           <CardFooter className="border-top d-flex">
    //             <div className="card-post__author d-flex">
    //               <a
    //                 href="#"
    //                 className="card-post__author-avatar card-post__author-avatar--small"
    //                 style={{backgroundImage: `url('${post.author.avatar}')`}}
    //               >
    //                 Written by James Khan
    //               </a>
    //               <div className="d-flex flex-column justify-content-center ml-3">
    //                 <span className="card-post__author-name">
    //                   {post.author.name}
    //                 </span>
    //                 <small className="text-muted">{post.date}</small>
    //               </div>
    //             </div>
    //             <div className="my-auto ml-auto">
    //               <Button size="sm" theme="white">
    //                 <i className="far fa-bookmark mr-1"/> Bookmark
    //               </Button>
    //             </div>
    //           </CardFooter>
    //         </Card>
    //       </Col>
    //     ))}
    //   </Row>
    // </Container>
  );
}
const styles = {
  cardBody: {
    height: '240px'
  },
  description: {
    fontSize: '15px',
    lineHeight: '1.5',
    textAlign: 'justify'
  }
}
export default withRouter(Posts)
