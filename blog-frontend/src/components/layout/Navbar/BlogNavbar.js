import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";
import {fetchPages, selectAllPages} from '../../../slices/pageSice'
import {Link, withRouter} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

const BlogNavbar = ({layout, stickyTop, location}) => {
  const dispatch = useDispatch()
  const pages = useSelector(selectAllPages)
  const [collapseOpen, setCollapseOpen] = useState(false);
  const toggleNavbar = () => setCollapseOpen(prevState => !prevState)
  // const classes = classNames(
  //   stickyTop && "sticky-top"
  // );
  useEffect(() => {
    dispatch(fetchPages())
  }, [])

  return (
    <header className="header-area">
      {/*<div className="top-header-area">*/}
      {/*  <div className="container">*/}
      {/*    <div className="row align-items-center">*/}
      {/*      <div className="col-12 col-md-6">*/}
      {/*        <div className="breaking-news-area d-flex align-items-center">*/}
      {/*          <div className="news-title">*/}
      {/*            <p>Breaking News:</p>*/}
      {/*          </div>*/}
      {/*          <div id="breakingNewsTicker" className="ticker">*/}
      {/*            <ul>*/}
      {/*              <li><a href="single-post.html">10 Things Amazon Echo Can Do</a></li>*/}
      {/*              <li><a href="single-post.html">Welcome to Colorlib Family.</a></li>*/}
      {/*              <li><a href="single-post.html">Boys 'doing well' after Thai</a></li>*/}
      {/*            </ul>*/}
      {/*          </div>*/}
      {/*        </div>*/}
      {/*      </div>*/}
      {/*      <div className="col-12 col-md-6">*/}
      {/*        <div className="top-meta-data d-flex align-items-center justify-content-end">*/}
      {/*          <div className="top-social-info">*/}
      {/*            <a href="#"><i className="fa fa-facebook"></i></a>*/}
      {/*            <a href="#"><i className="fa fa-twitter"></i></a>*/}
      {/*            <a href="#"><i className="fa fa-pinterest"></i></a>*/}
      {/*            <a href="#"><i className="fa fa-linkedin"></i></a>*/}
      {/*            <a href="#"><i className="fa fa-youtube-play"></i></a>*/}
      {/*          </div>*/}
      {/*          <div className="top-search-area">*/}
      {/*            <form action="/" method="post">*/}
      {/*              <input type="search" name="top-search" id="topSearch" placeholder="Search..."/>*/}
      {/*              <button type="submit" className="btn"><i className="fa fa-search" aria-hidden="true"></i></button>*/}
      {/*            </form>*/}
      {/*          </div>*/}
      {/*          <a href="login.html" className="login-btn"><i className="fa fa-user" aria-hidden="true"></i></a>*/}
      {/*        </div>*/}
      {/*      </div>*/}
      {/*    </div>*/}
      {/*  </div>*/}
      {/*</div>*/}

      <div className="vizew-main-menu" id="sticker">
        <div className="classy-nav-container breakpoint-off">
          <div className="container">
            <nav className="classy-navbar justify-content-between" id="vizewNav">
              <Link to="/" className="nav-brand"><img src={require("../../../assets/logo.png")} alt=""/></Link>
              <div className="classy-navbar-toggler">
                <span className="navbarToggler"><span></span><span></span><span></span></span>
              </div>
              <div className="classy-menu">
                <div className="classycloseIcon">
                  <div className="cross-wrap"><span className="top"></span><span className="bottom"></span></div>
                </div>
                <div className="classynav">
                  <ul>
                    <li className={`${location.pathname==='/'? 'active': ''}`}><Link to="/">Home</Link></li>
                    {pages.map((page, idx) => <li key={idx}>
                      <Link className={`${location.pathname===`/page/${page.slug}`? 'active': ''}`} to={`/page/${page.slug}`}>{page.title}</Link>
                    </li>)}
                  </ul>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header>
  );
};
BlogNavbar.propTypes = {
  /**
   * The layout type where the AdminNavbar is used.
   */
  layout: PropTypes.string,
  /**
   * Whether the main navbar is sticky to the top, or not.
   */
  stickyTop: PropTypes.bool
};
BlogNavbar.defaultProps = {
  stickyTop: true
};
export default withRouter(BlogNavbar);
