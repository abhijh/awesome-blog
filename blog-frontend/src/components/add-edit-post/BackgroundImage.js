import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Button, Card, CardImg, ListGroupItem} from "shards-react";

const BackgroundImage = ({backgroundImageUrlProp, setPostData}) => {
  const [backgroundImageUrl, setBackgroundImageUrl] = useState(backgroundImageUrlProp)
  const selectFileWithCKFinder = (e) => {
    e.preventDefault()
    window.CKFinder.modal({
      connectorPath: "/ckfinder/connector",
      chooseFiles: true,
      width: 800,
      height: 600,
      onInit: function( finder ) {
        finder.on( 'files:choose', function( evt ) {
          const file = evt.data.files.first();
          setPostData({'backgroundImage': file.getUrl()})
          setBackgroundImageUrl(file.getUrl())
        });

        finder.on( 'file:choose:resizedImage', function( evt ) {
          setPostData({'backgroundImage': evt.data.resizedUrl})
          setBackgroundImageUrl(evt.data.resizedUrl)
        });
      }
    });
  }

  return (
    <ListGroupItem className="d-flex justify-content-center px-3">
      <Card onClick={selectFileWithCKFinder}>
        {backgroundImageUrl ?
          <CardImg
            style={styles.imageStyle}
            src={backgroundImageUrl}/>: <Button>Select Image</Button>}
      </Card>
    </ListGroupItem>
  );
};

const styles = {
  imageStyle: {
    cursor: 'pointer',
    width: '100%',
    height: 'auto'
  }
}

BackgroundImage.propTypes = {
  setPostData: PropTypes.func.isRequired,
  backgroundImageUrlProp: PropTypes.string
};

export default BackgroundImage;
