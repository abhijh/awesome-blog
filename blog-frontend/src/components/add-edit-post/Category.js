import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

const animatedComponents = makeAnimated();
const categories = [
  {
    category: 'Technology',
    categoryTheme: 'royal-blue'
  },
  {
    category: 'Travel',
    categoryTheme: 'dark'
  },
  {
    category: 'Food',
    categoryTheme: 'warning'
  },
  {
    category: 'Design',
    categoryTheme: 'danger'
  }
];

const Category = ({category, categoryTheme, setPostData}) => {
  const selectedCategory = {
    category: category,
    categoryTheme: categoryTheme
  }
  const handleSelection = (selectedCategory) => {setPostData({...selectedCategory})}

  return (
      <Select
        style={{
          margin: '10px',
          padding: '10px'
        }}
        value={selectedCategory}
        onChange={handleSelection}
        components={animatedComponents}
        getOptionLabel={(option)=>option.category}
        getOptionValue={(option)=>option.categoryTheme}
        options={categories}/>
  );
};

Category.propTypes = {
  setPostData: PropTypes.func.isRequired
};

export default Category;
