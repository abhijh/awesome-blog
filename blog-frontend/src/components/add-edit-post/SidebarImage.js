import React from "react";
import PropTypes from "prop-types";
import {Card, CardBody, CardHeader, ListGroup} from "shards-react";
import BackgroundImage from "./BackgroundImage";

const SidebarImage = ({backgroundImageUrlProp, setPostData}) => {
  return (
    <Card small className="mb-3">
      <CardHeader className="border-bottom">
        <h6 className="m-0">Background Image</h6>
      </CardHeader>
      <CardBody className="p-0">
        <ListGroup flush>
          <BackgroundImage backgroundImageUrlProp={backgroundImageUrlProp} setPostData={setPostData} />
        </ListGroup>
      </CardBody>
    </Card>
  );
}

SidebarImage.propTypes = {
  setPostData: PropTypes.func.isRequired
};

export default SidebarImage;
