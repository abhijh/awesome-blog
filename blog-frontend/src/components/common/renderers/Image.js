import React from "react";

const Image = (props) => {
  return <img
    {...props} style={{maxWidth: '100px'}}
    alt={props.alt? props.alt: 'blog image'} />
}

export default Image;