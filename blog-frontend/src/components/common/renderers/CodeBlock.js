import React from "react";
import PropTypes from "prop-types";
import {Prism as SyntaxHighlighter} from 'react-syntax-highlighter';
import {darcula} from 'react-syntax-highlighter/dist/esm/styles/prism';
import './codeblock.css'

const CodeBlock = (props) => {
  const {language, value} = props;
  return (
    <SyntaxHighlighter
      language={language}
      style={{
        ...darcula
      }}>
      {value}
    </SyntaxHighlighter>
  );
}

CodeBlock.propTypes = {
  value: PropTypes.string.isRequired,
  language: PropTypes.string
}

CodeBlock.defaultProps = {
  language: null
}

export default CodeBlock;