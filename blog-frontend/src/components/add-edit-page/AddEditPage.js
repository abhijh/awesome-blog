import React, {useState} from "react"
import PropTypes from "prop-types";

import {Card, CardBody, Col, Container, FormInput, Row} from "shards-react"
import PageTitle from "../common/PageTitle"
import Editor from "../common/editor/Editor"
import SidebarActions from "../common/SidebarActions"

const AddEditPage = ({pageProp, onSavePage}) => {
  const [page, setPage] = useState(pageProp)

  const handleOnChange = (e) => {
    const datum = Object
    datum[e.target.name] = e.target.value
    setPageData(datum)
    e.persist()
  }

  const setPageData = (data) => {
    setPage((prevState) => {
      return {
        ...prevState,
        ...data
      }
    })
  }

  return (
    <Container fluid className="main-content-container px-4 pb-4">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title={pageProp.title} subtitle={pageProp.title===""? "Add Page": "Edit Page"} className="text-sm-left" />
      </Row>
      <Row>
        {/* Editor */}
        <Col lg="9" md="12">
          <div className="add-new-post">
            <Card small className="mb-3">
              <CardBody>
                <FormInput
                  onChange={handleOnChange}
                  value={page.title}
                  name="title"
                  size="lg"
                  className="mb-3"
                  placeholder="Page Title"/>
                <Editor height="616px" value={page.body} setData={setPageData}/>
              </CardBody>
            </Card>
          </div>
        </Col>
        {/* Sidebar Widgets */}
        <Col lg="3" md="12">
          <SidebarActions
            {...pageProp}
            onSavePostAsDraft={() => onSavePage(page, true)}
            onPublishPost={() => onSavePage(page, false)}/>
        </Col>
      </Row>
    </Container>
  );
}

AddEditPage.propTypes = {
  pageProp: PropTypes.object.isRequired
};

AddEditPage.defaultProps = {
  pageProp: {
    draft: true,
    title: '',
    body: ''
  }
};

export default AddEditPage;
