const LocalStorageService = (function(){
  let _service;
  function _getService() {
    if(!_service) {
      _service = this;
      return _service
    }
    return _service
  }

  function _setAccessToken(accessToken) {
    localStorage.setItem('access_token', accessToken);
  }

  function _getAccessToken() {
    return localStorage.getItem('access_token');
  }

  function _clearToken() {
    localStorage.removeItem('access_token');
  }

  return {
    getService : _getService,
    setAccessToken : _setAccessToken,
    getAccessToken : _getAccessToken,
    clearToken : _clearToken
  }
})();

export default LocalStorageService;