package xyz.abhinav.blog.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import xyz.abhinav.blog.db.entity.Tag;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TagDTO {
    private Long id;
    private String name;
    private String theme;

    public Tag toEntity() {
        return Tag.builder()
                .id(id)
                .name(name)
                .theme(theme)
                .build();
    }
}
