package xyz.abhinav.blog.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class GreetDTO {
    @JsonProperty
    String message;
}
