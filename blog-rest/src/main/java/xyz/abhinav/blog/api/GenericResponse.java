package xyz.abhinav.blog.api;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class GenericResponse<T> {
    private String message;
    private T data;
}
