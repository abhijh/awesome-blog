package xyz.abhinav.blog.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import xyz.abhinav.blog.db.entity.Image;

@Builder
public class ImageDTO {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String path;

    public Image toEntity() {
        return Image.builder()
                .id(id)
                .path(path)
                .build();
    }
}
