package xyz.abhinav.blog.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import xyz.abhinav.blog.db.entity.Post;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostDTO {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String slug;
    @JsonProperty
    private String backgroundImage;
    @JsonProperty
    private String category;
    @JsonProperty
    private String categoryTheme;
    @JsonProperty
    private UserDTO author;
    @JsonProperty
    private String title;
    @JsonProperty
    private String body;
    @JsonProperty
    private String description;
    @JsonProperty
    private Boolean draft;
    @JsonProperty
    private DateTime createdDate;
    @JsonProperty
    private DateTime updatedDate;
    private Set<TagDTO> tags;

    public Post toEntity() {
        return Post.builder()
                .id(id)
                .author(author != null? author.toEntity(): null)
                .slug(title.toLowerCase().replace(" ", "-"))
                .backgroundImage(backgroundImage)
                .category(category)
                .categoryTheme(categoryTheme)
                .title(title)
                .description(description)
                .body(body)
                .draft(draft)
                .createdDate(createdDate)
                .updatedDate(updatedDate)
                .tags(tags.stream()
                        .map(TagDTO::toEntity)
                        .collect(Collectors.toSet()))
                .build();
    }
}
