package xyz.abhinav.blog.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import xyz.abhinav.auth.core.entities.AuthRole;
import xyz.abhinav.blog.db.entity.Role;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolesDTO {
    @JsonProperty
    private List<String> roles;

    public Set<AuthRole> toEntity() {
        return roles.stream().map(this::toRole).collect(Collectors.toSet());
    }

    private Role toRole(String name) {
        return Role.builder()
                .name(name)
                .build();
    }
}
