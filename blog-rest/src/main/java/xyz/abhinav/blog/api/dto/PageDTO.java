package xyz.abhinav.blog.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import xyz.abhinav.blog.db.entity.Page;
import xyz.abhinav.blog.db.entity.Post;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageDTO {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String slug;
    @JsonProperty
    private String title;
    @JsonProperty
    private String body;
    @JsonProperty
    private Boolean draft;
    @JsonProperty
    private DateTime createdDate;
    @JsonProperty
    private DateTime updatedDate;

    public Page toEntity() {
        return Page.builder()
                .id(id)
                .slug(title.toLowerCase().replace(" ", "-"))
                .title(title)
                .body(body)
                .draft(draft)
                .createdDate(createdDate)
                .updatedDate(updatedDate)
                .build();
    }
}
