package xyz.abhinav.blog.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import xyz.abhinav.blog.db.entity.User;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String email;
    @JsonProperty
    private String avatar;
    @JsonProperty
    private RolesDTO roles;

    public User toEntity() {
        return User.builder()
                .id(id)
                .name(name)
                .email(email)
                .avatar(avatar)
                .roles(roles.toEntity())
                .build();
    }
}
