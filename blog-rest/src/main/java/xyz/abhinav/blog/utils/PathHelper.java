package xyz.abhinav.blog.utils;

import com.google.common.base.Preconditions;

public class PathHelper {
    private PathHelper() {
    }
    private static String mediaURL;
    private static String mediaRoot;

    public static void setMediaURL(String mediaURL) {
        PathHelper.mediaURL = mediaURL;
    }

    public static String getMediaURLPath(String path) {
        Preconditions.checkNotNull(mediaURL);
        return mediaURL + path;
    }

    public static void setMediaRoot(String mediaRoot) {
        PathHelper.mediaRoot = mediaRoot;
    }

    public static String getMediaPath() {
        Preconditions.checkNotNull(mediaRoot);
        return mediaRoot;
    }
}
