
package xyz.abhinav.blog.db.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import xyz.abhinav.blog.api.dto.ImageDTO;
import xyz.abhinav.blog.utils.PathHelper;

import javax.persistence.*;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "IMAGES")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    private Long id;
    @JsonProperty
    @Column
    private String path;

    public ImageDTO toDTO() {
        return ImageDTO.builder()
                .id(id)
                .path(PathHelper.getMediaURLPath(path))
                .build();
    }
}
