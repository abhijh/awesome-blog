package xyz.abhinav.blog.db.repository;

import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import xyz.abhinav.blog.db.entity.Page;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class PageRepository extends AbstractDAO<Page> {
    @Inject
    public PageRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<Page> findById(Long id) {
        return Optional.ofNullable(get(id));
    }

    public Optional<Page> update(Page page) {
        return Optional.ofNullable(persist(page));
    }

    public List<Page> list() {
        return list(criteria());
    }

    public Page save(Page page) {
        return get(currentSession().save(page));
    }

    public Optional<Page> findPublishedPageBySlug(String slug) {
        Criteria criteria = criteria();
        criteria.add(Restrictions.eq("slug", slug));
        criteria.add(Restrictions.eq("draft", false));
        return Optional.ofNullable(uniqueResult(criteria));
    }

    public List<Page> findPublishedPages() {
        Criteria criteria = criteria();
        criteria.add(Restrictions.eq("draft", false));
        return list(criteria);
    }

    public void delete(Long id) {
        this.currentSession().delete(Page.builder()
                .id(id)
                .build());
    }
}
