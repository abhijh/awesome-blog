package xyz.abhinav.blog.db.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import xyz.abhinav.blog.api.dto.PageDTO;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "slug")
@Entity
@Table(name = "PAGES")
public class Page {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    private Long id;
    @JsonProperty
    @Column(updatable = false)
    private String slug;
    @Column
    private String title;
    @Column
    @Type(type="text")
    private String body;
    @Column
    private Boolean draft;
    @Column(insertable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime createdDate;
    @Column(insertable = false, updatable = false)
    @GeneratedValue
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime updatedDate;

    public PageDTO toDTO() {
        return PageDTO.builder()
                .id(id)
                .slug(slug)
                .title(title)
                .body(body)
                .createdDate(createdDate)
                .updatedDate(updatedDate)
                .draft(draft)
                .build();
    }
}
