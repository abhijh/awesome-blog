package xyz.abhinav.blog.db.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import xyz.abhinav.blog.api.dto.PostDTO;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "slug")
@Entity
@Table(name = "POSTS")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    private Long id;
    @JsonProperty
    @Column(updatable = false)
    private String slug;
    @ManyToOne
    private User author;
    @Column
    private String backgroundImage;
    @Column
    private String category;
    @Column
    private String categoryTheme;
    @Column
    private String title;
    @Column
    @Type(type="text")
    private String description;
    @Column
    @Type(type="text")
    private String body;
    @Column
    private Boolean draft;
    @Column(insertable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime createdDate;
    @Column(insertable = false, updatable = false)
    @GeneratedValue
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime updatedDate;
    @JsonProperty
    @Column
    @ManyToMany(fetch = FetchType.EAGER,
            targetEntity = Tag.class,
            cascade = CascadeType.ALL)
    @JoinTable(name="POST_TAGS",
            joinColumns=@JoinColumn(name="POST_ID"),
            inverseJoinColumns=@JoinColumn(name="TAG_ID"))
    private Set<Tag> tags = new HashSet<>();

    public PostDTO toDTO() {
        return PostDTO.builder()
                .id(id)
                .slug(slug)
                .author(author.toDTO())
                .backgroundImage(backgroundImage)
                .category(category)
                .categoryTheme(categoryTheme)
                .title(title)
                .description(description)
                .body(body)
                .createdDate(createdDate)
                .updatedDate(updatedDate)
                .draft(draft)
                .tags(tags.stream()
                        .map(Tag::toDTO)
                        .collect(Collectors.toSet()))
                .build();
    }
}
