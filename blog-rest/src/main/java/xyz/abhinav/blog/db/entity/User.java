package xyz.abhinav.blog.db.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import xyz.abhinav.auth.core.entities.AuthRole;
import xyz.abhinav.auth.core.entities.AuthUser;
import xyz.abhinav.blog.api.dto.RolesDTO;
import xyz.abhinav.blog.api.dto.UserDTO;

import javax.persistence.*;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "email")
@Entity
@Table(name = "USERS")
@NamedQuery(name = User.FIND_BY_USERNAME_PASSWORD, query = User.FIND_BY_USERNAME_PASSWORD_QUERY)
public class User implements Principal, AuthUser {
    public static final String FIND_BY_USERNAME_PASSWORD = "FIND_BY_USERNAME_PASSWORD";
    public static final String FIND_BY_USERNAME_PASSWORD_QUERY = "select u from User u where u.email = :email and u.password = :password";

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @JsonProperty
    private Long id;
    @Column @JsonProperty
    private String name;
    @Column @JsonProperty
    private String email;
    @Column @JsonProperty
    private String password;
    @Column @JsonProperty
    private String avatar;
    @JsonProperty
    @Column
    @ManyToMany(fetch = FetchType.EAGER,
            targetEntity = Role.class,
            cascade = CascadeType.ALL)
    @JoinTable(name="USER_ROLES",
            joinColumns=@JoinColumn(name="USER_ID"),
            inverseJoinColumns=@JoinColumn(name="ROLE_ID"))
    private Set<AuthRole> roles = new HashSet<>();

    @Column
    @OneToMany(
            fetch = FetchType.EAGER,
            targetEntity = Post.class,
            mappedBy = "author",
            cascade = CascadeType.ALL)
    private Set<Post> posts = new HashSet<>();

    public UserDTO toDTO() {
        return UserDTO.builder()
                .id(id)
                .email(email)
                .name(name)
                .avatar(avatar)
                .roles(RolesDTO.builder()
                        .roles(roles.stream().map(AuthRole::getName).collect(Collectors.toList()))
                        .build()
                ).build();
    }
}
