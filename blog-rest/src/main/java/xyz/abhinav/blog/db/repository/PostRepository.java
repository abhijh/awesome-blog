package xyz.abhinav.blog.db.repository;

import io.dropwizard.hibernate.AbstractDAO;
import javafx.geometry.Pos;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import xyz.abhinav.blog.db.entity.Post;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class PostRepository extends AbstractDAO<Post> {
    @Inject
    public PostRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<Post> findById(Long id) {
        return Optional.ofNullable(get(id));
    }

    public Optional<Post> update(Post post) {
        return Optional.ofNullable(persist(post));
    }

    public List<Post> list() {
        return list(criteria());
    }

    public Post save(Post post) {
        return get(currentSession().save(post));
    }

    public Optional<Post> findPublishedPostsBySlug(String slug) {
        Criteria criteria = criteria();
        criteria.add(Restrictions.eq("slug", slug));
        criteria.add(Restrictions.eq("draft", false));
        return Optional.ofNullable(uniqueResult(criteria));
    }

    public List<Post> findPublishedPosts() {
        Criteria criteria = criteria();
        criteria.add(Restrictions.eq("draft", false));
        return list(criteria);
    }

    public void delete(Long id) {
        this.currentSession().delete(Post.builder()
                .id(id)
                .build());
    }
}
