package xyz.abhinav.blog.db.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import xyz.abhinav.blog.api.dto.TagDTO;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "name")
@Entity
@Table(name = "TAGS")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    private Long id;
    @Column
    private String name;
    @Column
    private String theme;
    @JsonProperty
    @Column
    @ManyToMany(
            mappedBy = "tags",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private Set<Post> posts = new HashSet<>();

    public TagDTO toDTO() {
        return TagDTO.builder()
                .id(id)
                .name(name)
                .theme(theme)
                .build();
    }
}
