package xyz.abhinav.blog.db.repository;

import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import xyz.abhinav.blog.db.entity.Image;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class ImageRepository extends AbstractDAO<Image> {
    @Inject
    public ImageRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<Image> findById(Long id) {
        return Optional.ofNullable(get(id));
    }

    public List<Image> list() {
        return list(criteria());
    }

    public Image save(Image image) {
        return persist(image);
    }
}
