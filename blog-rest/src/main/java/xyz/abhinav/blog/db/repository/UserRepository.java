package xyz.abhinav.blog.db.repository;

import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import xyz.abhinav.auth.dao.IUserDao;
import xyz.abhinav.blog.db.entity.User;

import javax.inject.Inject;
import javax.persistence.NoResultException;

public class UserRepository extends AbstractDAO<User> implements IUserDao {
    @Inject
    public UserRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public User findById(Long id) {
        return get(id);
    }

    public User findByEmailAndPassword(String email, String password) throws NoResultException {
        Query query = namedQuery(User.FIND_BY_USERNAME_PASSWORD);
        query.setParameter("email", email);
        query.setParameter("password", password);
        return (User) query.getSingleResult();
    }

    @Override
    public User findByEmail(String email) {
        Criteria criteria = criteria();
        criteria.add(Restrictions.eq("email", email));
        return uniqueResult(criteria);
    }

    public User create(User user) {
        return persist(user);
    }
}
