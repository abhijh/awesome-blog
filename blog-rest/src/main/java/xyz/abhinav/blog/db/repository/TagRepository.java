package xyz.abhinav.blog.db.repository;

import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import xyz.abhinav.blog.db.entity.Tag;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class TagRepository extends AbstractDAO<Tag> {
    @Inject
    public TagRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<Tag> findById(Long id) {
        return Optional.ofNullable(get(id));
    }

    public Optional<Tag> update(Tag tag) {
        return Optional.ofNullable(persist(tag));
    }

    public List<Tag> list() {
        return list(criteria());
    }

    public Tag save(Tag tag) {
        return get(currentSession().save(tag));
    }

    public void delete(Long id) {
        this.currentSession().delete(Tag.builder()
                .id(id)
                .build());
    }
}
