package xyz.abhinav.blog;

public interface BlogRestConfiguration {
    String getMediaRoot();
    String getMediaURL();
}
