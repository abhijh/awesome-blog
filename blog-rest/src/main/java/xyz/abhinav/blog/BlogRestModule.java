package xyz.abhinav.blog;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import io.dropwizard.Configuration;
import io.dropwizard.hibernate.HibernateBundle;
import lombok.AllArgsConstructor;
import org.hibernate.SessionFactory;
import xyz.abhinav.blog.utils.PathHelper;

@AllArgsConstructor
public abstract class BlogRestModule<T extends Configuration & BlogRestConfiguration>
        extends AbstractModule {

    private final HibernateBundle<T> hibernateBundle;

    @Override
    protected void configure() {
        bind(SessionFactory.class).toInstance(hibernateBundle.getSessionFactory());
    }

    @Provides
    @Named("mediaRoot")
    public String mediaRoot(T configuration) {
        String mediaRoot = configuration.getMediaRoot();
        PathHelper.setMediaRoot(mediaRoot);
        return mediaRoot;
    }

    @Provides
    @Named("mediaURL")
    public String mediaURL(T configuration) {
        String mediaURL = configuration.getMediaURL();
        PathHelper.setMediaURL(mediaURL);
        return mediaURL;
    }
}
