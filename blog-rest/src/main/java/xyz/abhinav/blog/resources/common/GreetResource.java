package xyz.abhinav.blog.resources.common;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.dropwizard.auth.Auth;
import io.swagger.annotations.*;
import lombok.NoArgsConstructor;
import xyz.abhinav.blog.api.dto.GreetDTO;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import xyz.abhinav.blog.api.dto.UserDTO;
import xyz.abhinav.blog.db.entity.User;

@SwaggerDefinition(
        securityDefinition = @SecurityDefinition(
                apiKeyAuthDefinitions = {
                        @ApiKeyAuthDefinition(
                                key = "Bearer",
                                in = ApiKeyAuthDefinition.ApiKeyLocation.HEADER,
                                name = "Authorization"
                        )
                }
        )
)
@Api("Api Status Endpoint")
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Singleton
@Produces({"application/json"})
@NoArgsConstructor(onConstructor = @__({@Inject}))
public class GreetResource {
    @GET
    public Response greet() {
        return Response.ok()
                .entity(GreetDTO.builder().message("API is running!").build())
                .build();
    }

    @GET
    @Path("/me")
    @ApiOperation(
            value = "Get user details",
            notes = "Get user details",
            authorizations={@Authorization("Bearer")},
            response = UserDTO.class)
    public Response me(@ApiParam(hidden = true) @Auth User user) {
        return Response.ok()
                .entity(user.toDTO())
                .build();
    }
}
