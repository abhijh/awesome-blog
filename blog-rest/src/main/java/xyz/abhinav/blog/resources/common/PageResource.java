package xyz.abhinav.blog.resources.common;

import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import xyz.abhinav.blog.db.entity.Page;
import xyz.abhinav.blog.db.repository.PageRepository;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;

@Api("Page Resources")
@Resource
@Path("/page")
@Consumes({"application/json"})
@Produces({"application/json"})
@AllArgsConstructor(onConstructor = @__(@Inject))
public class PageResource {
    private final PageRepository pageRepository;

    @GET
    @Path("/{slug}")
    @UnitOfWork
    public Response findPublishedPostsBySlug(@PathParam("slug") String slug) {
        return Response.ok()
                .entity(pageRepository.findPublishedPageBySlug(slug).orElseGet(() -> {
                    throw new NotFoundException("Page Not Found");
                }).toDTO()).build();
    }

    @GET
    @UnitOfWork
    public Response findPublishedPosts() {
        return Response.ok()
                .entity(pageRepository.findPublishedPages()
                        .stream()
                        .map(Page::toDTO)
                        .collect(Collectors.toList()))
                .build();
    }
}
