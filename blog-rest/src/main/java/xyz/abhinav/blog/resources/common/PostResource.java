package xyz.abhinav.blog.resources.common;

import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import xyz.abhinav.blog.db.entity.Post;
import xyz.abhinav.blog.db.repository.PostRepository;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;

@Api("Post Resources")
@Resource
@Path("/post")
@Consumes({"application/json"})
@Produces({"application/json"})
@AllArgsConstructor(onConstructor = @__(@Inject))
public class PostResource {
    private final PostRepository postRepository;

    @GET
    @Path("/{slug}")
    @UnitOfWork
    public Response findPublishedPostsBySlug(@PathParam("slug") String slug) {
        return Response.ok()
                .entity(postRepository.findPublishedPostsBySlug(slug).orElseGet(() -> {
                    throw new NotFoundException("Post Not Found");
                }).toDTO()).build();
    }

    @GET
    @UnitOfWork
    public Response findPublishedPosts() {
        return Response.ok()
                .entity(postRepository.findPublishedPosts()
                        .stream()
                        .map(Post::toDTO)
                        .collect(Collectors.toList()))
                .build();
    }
}
