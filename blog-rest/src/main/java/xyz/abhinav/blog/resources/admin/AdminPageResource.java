package xyz.abhinav.blog.resources.admin;

import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import xyz.abhinav.blog.api.GenericResponse;
import xyz.abhinav.blog.api.dto.PageDTO;
import xyz.abhinav.blog.db.entity.Page;
import xyz.abhinav.blog.db.repository.PageRepository;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;

@Api(value = "Admin Resources", authorizations = {@Authorization("Bearer")})
@Resource
@RolesAllowed({"ADMIN"})
@Path("/admin/page")
@Consumes({"application/json"})
@Produces({"application/json"})
@AllArgsConstructor(onConstructor = @__(@Inject))
public class AdminPageResource {
    private final PageRepository pageRepository;

    @GET
    @UnitOfWork
    public Response list() {
        return Response.ok()
                .entity(GenericResponse.builder()
                        .data(pageRepository.list()
                                .stream()
                                .map(Page::toDTO)
                                .collect(Collectors.toList()))
                        .message("Pages fetched successfully")
                        .build())
                .build();
    }

    @GET
    @Path("/{id}")
    @UnitOfWork
    public Response findById(@PathParam("id") Long id) {
        Page page = pageRepository.findById(id).orElseGet(() -> {
            throw new NotFoundException("Page Not Found");
        });
        return Response.ok()
                .entity(GenericResponse.builder()
                        .data(page.toDTO())
                        .message(String.format("Page %s fetched successfully", page.getTitle()))
                        .build())
                .build();
    }

    @POST
    @UnitOfWork
    public Response save(PageDTO pageDTO) {
        try {
            Page page = pageRepository.save(pageDTO.toEntity());
            pageDTO = page.toDTO();
            return Response.ok()
                    .entity(GenericResponse.builder()
                            .data(pageDTO)
                            .message(String.format("Page %s saved successfully", page.getTitle()))
                            .build())
                    .build();
        } catch (Exception e) {
            throw new BadRequestException("Could not save, " + e.getCause().getMessage());
        }
    }

    @PUT
    @UnitOfWork
    public Response update(PageDTO pageDTO) {
        Page page = pageRepository.update(pageDTO.toEntity()).orElseGet(() -> {
            throw new NotFoundException("Could not update page");
        });
        return Response.ok()
                .entity(GenericResponse.builder()
                        .data(page.toDTO())
                        .message(String.format("Page %s updated successfully", page.getTitle()))
                        .build())
                .build();
    }

    @DELETE
    @UnitOfWork
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) {
        try {
            pageRepository.delete(id);
            return Response.ok()
                    .entity(GenericResponse.builder()
                            .message("Page deleted successfully")
                            .build())
                    .build();
        } catch (ConstraintViolationException e) {
            throw new BadRequestException("Could not delete, " + e.getCause().getMessage());
        }
    }
}
