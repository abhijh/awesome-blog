package xyz.abhinav.blog.resources.admin;

import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import xyz.abhinav.blog.services.ImageService;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;

@Api(value = "Admin Resources", authorizations = {@Authorization("Bearer")})
@Resource
@RolesAllowed({"ADMIN"})
@Path("/admin/image")
@Consumes({"application/json"})
@Produces({"application/json"})
@AllArgsConstructor(onConstructor = @__({@Inject}))
public class AdminImageResource {
    private final ImageService imageService;

    @UnitOfWork
    @GET
    public Response list() {
        return Response.ok()
                .entity(imageService.list())
                .build();
    }

    @UnitOfWork
    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") Long id) {
        return Response.ok()
                .entity(imageService.findById(id))
                .build();
    }

    @UnitOfWork
    @POST
    @Path("/save")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response save(@FormDataParam("image") final InputStream imageInputStream,
                         @FormDataParam("image") final FormDataContentDisposition imageFileDetail) {
        return Response.ok()
                .entity(imageService.save(imageInputStream, imageFileDetail))
                .build();
    }
}
