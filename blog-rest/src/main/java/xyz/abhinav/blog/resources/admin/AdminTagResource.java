package xyz.abhinav.blog.resources.admin;

import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import xyz.abhinav.blog.api.GenericResponse;
import xyz.abhinav.blog.api.dto.TagDTO;
import xyz.abhinav.blog.db.entity.Tag;
import xyz.abhinav.blog.db.repository.TagRepository;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;

@Api(value = "Admin Resources", authorizations = {@Authorization("Bearer")})
@Resource
@RolesAllowed({"ADMIN"})
@Path("/admin/tag")
@Consumes({"application/json"})
@Produces({"application/json"})
@AllArgsConstructor(onConstructor = @__(@Inject))
public class AdminTagResource {
    private final TagRepository tagRepository;

    @GET
    @UnitOfWork
    public Response list() {
        return Response.ok()
                .entity(GenericResponse.builder()
                        .data(tagRepository.list()
                                .stream()
                                .map(Tag::toDTO)
                                .collect(Collectors.toList()))
                        .message("Tags fetched successfully")
                        .build())
                .build();
    }

    @POST
    @UnitOfWork
    public Response save(TagDTO tagDTO) {
        try {
            Tag tag = tagRepository.save(tagDTO.toEntity());
            return Response.ok()
                    .entity(GenericResponse.builder()
                            .data(tag.toDTO())
                            .message(String.format("Tag %s saved successfully", tag.getName()))
                            .build())
                    .build();
        } catch (Exception e) {
            throw new BadRequestException("Could not save, " + e.getMessage());
        }
    }

    @PUT
    @UnitOfWork
    public Response update(TagDTO tagDTO) {
        Tag tag = tagRepository.update(tagDTO.toEntity()).orElseGet(() -> {
            throw new NotFoundException("Could not update tag");
        });
        return Response.ok()
                .entity(GenericResponse.builder()
                        .data(tag.toDTO())
                        .message(String.format("Tag %s updated successfully", tag.getName()))
                        .build())
                .build();
    }

    @UnitOfWork
    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) {
        try {
            tagRepository.delete(id);
            return Response.ok()
                    .entity(GenericResponse.builder()
                            .message("Tag deleted successfully")
                            .build())
                    .build();
        } catch (ConstraintViolationException e) {
            throw new BadRequestException("Could not delete, " + e.getCause().getMessage());
        }
    }
}
