package xyz.abhinav.blog.resources.admin;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import xyz.abhinav.blog.api.GenericResponse;
import xyz.abhinav.blog.api.dto.PostDTO;
import xyz.abhinav.blog.db.entity.Post;
import xyz.abhinav.blog.db.entity.User;
import xyz.abhinav.blog.db.repository.PostRepository;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;

@Api(value = "Admin Resources", authorizations = {@Authorization("Bearer")})
@Resource
@RolesAllowed({"ADMIN"})
@Path("/admin/post")
@Consumes({"application/json"})
@Produces({"application/json"})
@AllArgsConstructor(onConstructor = @__(@Inject))
public class AdminPostResource {
    private final PostRepository postRepository;

    @GET
    @UnitOfWork
    public Response list() {
        return Response.ok()
                .entity(GenericResponse.builder()
                        .data(postRepository.list()
                                .stream()
                                .map(Post::toDTO)
                                .collect(Collectors.toList()))
                        .message("Posts fetched successfully")
                        .build())
                .build();
    }

    @GET
    @Path("/{id}")
    @UnitOfWork
    public Response findById(@PathParam("id") Long id) {
        Post post = postRepository.findById(id).orElseGet(() -> {
            throw new NotFoundException("Post Not Found");
        });
        return Response.ok()
                .entity(GenericResponse.builder()
                        .data(post.toDTO())
                        .message(String.format("Post %s fetched successfully", post.getTitle()))
                        .build())
                .build();
    }

    @POST
    @UnitOfWork
    public Response save(@ApiParam(hidden = true) @Auth User user, PostDTO postDTO) {
        try {
            postDTO.setAuthor(user.toDTO());
            Post post = postRepository.save(postDTO.toEntity());
            return Response.ok()
                    .entity(GenericResponse.builder()
                            .data(post.toDTO())
                            .message(String.format("Post %s saved successfully", post.getTitle()))
                            .build())
                    .build();
        } catch (Exception e) {
            throw new BadRequestException("Could not save, " + e.getMessage());
        }
    }

    @PUT
    @UnitOfWork
    public Response update(PostDTO postDTO) {
        Post post = postRepository.update(postDTO.toEntity()).orElseGet(() -> {
            throw new NotFoundException("Could not update post");
        });
        return Response.ok()
                .entity(GenericResponse.builder()
                        .data(post.toDTO())
                        .message(String.format("Post %s updated successfully", post.getTitle()))
                        .build())
                .build();
    }

    @UnitOfWork
    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) {
        try {
            postRepository.delete(id);
            return Response.ok()
                    .entity(GenericResponse.builder()
                            .message("Post deleted successfully")
                            .build())
                    .build();
        } catch (ConstraintViolationException e) {
            throw new BadRequestException("Could not delete, " + e.getCause().getMessage());
        }
    }
}
