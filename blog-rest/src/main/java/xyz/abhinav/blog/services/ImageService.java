package xyz.abhinav.blog.services;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import xyz.abhinav.blog.api.dto.ImageDTO;
import xyz.abhinav.blog.db.entity.Image;
import xyz.abhinav.blog.db.repository.ImageRepository;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.io.*;
import java.rmi.ServerException;
import java.util.List;
import java.util.stream.Collectors;

public class ImageService {
    private final ImageRepository imageRepository;
    private final String mediaRoot;

    @Inject
    public ImageService(ImageRepository imageRepository,
                        @Named("mediaRoot") String mediaRoot,
                        @Named("mediaURL") String mediaURL) {
        this.imageRepository = imageRepository;
        this.mediaRoot = mediaRoot;
    }

    public List<ImageDTO> list() {
        return imageRepository.list()
                .stream()
                .map(Image::toDTO)
                .collect(Collectors.toList());
    }

    public ImageDTO findById(Long id) {
        return imageRepository.findById(id).orElseGet(() -> {
                    throw new NotFoundException("Post Not Found");
                }).toDTO();
    }

    public ImageDTO save(final InputStream imageInputStream,
                         final FormDataContentDisposition imageFileDetail) {
        try {
            String fileName = imageFileDetail.getFileName();
            writeToFile(
                    imageInputStream,
                    mediaRoot + fileName);
            Image image = Image.builder()
                    .path(fileName)
                    .build();
            image = imageRepository.save(image);
            return image.toDTO();
        } catch (Exception e) {
            throw new BadRequestException(e.getCause().toString());
        }
    }

    private void writeToFile(InputStream uploadedInputStream,
                             String uploadedFileLocation) throws ServerException {
        File file = new File(uploadedFileLocation);
        try (OutputStream out = new FileOutputStream(file)) {
            int read = 0;
            final int BUFFER_LENGTH = 1024;
            final int UPLOAD_SIZE_LIMIT = 102400;

            byte[] bytes = new byte[BUFFER_LENGTH];
            int size = 0;
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
                size = size + read;
                if (size > UPLOAD_SIZE_LIMIT) {
                    throw new BadRequestException("File size exceeds limit.");
                }
            }
        } catch (IOException e) {
            throw new ServerException(e.getMessage(), e);
        }
    }
}
