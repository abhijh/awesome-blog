package xyz.abhinav.blog;

import com.google.common.util.concurrent.UncaughtExceptionHandlers;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.bundles.assets.ConfiguredAssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.dhatim.dropwizard.sentry.logging.SentryBootstrap;
import org.eclipse.jetty.server.session.SessionHandler;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import ru.vyarus.guicey.spa.SpaBundle;
import xyz.abhinav.auth.JwtAuthBundle;
import xyz.abhinav.blog.db.entity.*;

import javax.servlet.MultipartConfigElement;
import java.util.Collections;
import java.util.List;

import xyz.abhinav.blog.db.repository.UserRepository;

public class BlogApplication extends Application<BlogConfiguration> {
    private static final String DSN = "https://28b15e7a12ef475687a123edecf90ff9@o385434.ingest.sentry.io/5218157";
    private static final String THRESHOLD = "ERROR";
    private static final String ENVIRONMENT = System.getenv("ENVIRONMENT");
    private static final HibernateBundle<BlogConfiguration> hibernateBundle = new HibernateBundle<BlogConfiguration>(
            User.class,
            Role.class,
            Post.class,
            Page.class,
            Image.class,
            Tag.class
    ) {
        public PooledDataSourceFactory getDataSourceFactory(BlogConfiguration configuration) {
            return configuration.database;
        }
    };
    private static final JwtAuthBundle<BlogConfiguration> jwtAuthBundle = new JwtAuthBundle<>(hibernateBundle, UserRepository.class);
    private static final ConfiguredAssetsBundle configuredAssetsBundle = new ConfiguredAssetsBundle();
    private static final BlogRestBundle<BlogConfiguration> blogRestBundle = new BlogRestBundle<>();
    private static final MultiPartBundle multiPartBundle = new MultiPartBundle();
    private static final SwaggerBundle<BlogConfiguration> swaggerBundle = new SwaggerBundle<BlogConfiguration>() {
        @Override
        protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(BlogConfiguration configuration) {
            return configuration.swaggerBundleConfiguration;
        }
    };
    private static final MigrationsBundle<BlogConfiguration> migrationsBundle = new MigrationsBundle<BlogConfiguration>() {
        public DataSourceFactory getDataSourceFactory(BlogConfiguration configuration) {
            return configuration.database;
        }
    };
    private static final List<String> PACKAGE_NAME_LIST = Collections.singletonList("xyz.abhinav.blog");
    private static final GuiceBundle guiceBundle = GuiceBundle.builder()
            .bundles(
                SpaBundle.app("app", "/app", "/").build(),
                SpaBundle.app("manager", "/manager", "/manager/").build()
            )
            .enableAutoConfig(PACKAGE_NAME_LIST.toArray(new String[0]))
            .modules(new BlogApplicationModule() {}, new BlogRestModule<BlogConfiguration>(hibernateBundle) {})
            .build();
    private static final String NAME = "com.cksource.ckfinder.servlet.CKFinderServlet";

    public static void main(final String[] args) throws Exception {
        SentryBootstrap.Builder
                .withDsn(DSN)
                .withThreshold(THRESHOLD)
                .withEnvironment(ENVIRONMENT == null? "Local": ENVIRONMENT)
                .bootstrap();
        Thread.currentThread().setUncaughtExceptionHandler(UncaughtExceptionHandlers.systemExit());
        new BlogApplication().run(args);
    }

    @Override
    public String getName() {
        return "Blog";
    }

    @Override
    public void initialize(final Bootstrap<BlogConfiguration> bootstrap) {
        bootstrap.addBundle(configuredAssetsBundle);
        bootstrap.addBundle(blogRestBundle);
        bootstrap.addBundle(hibernateBundle);
        bootstrap.addBundle(migrationsBundle);
        bootstrap.addBundle(jwtAuthBundle);
        bootstrap.addBundle(swaggerBundle);
        bootstrap.addBundle(guiceBundle);
        bootstrap.addBundle(multiPartBundle);
    }

    @Override
    public void run(final BlogConfiguration configuration,
                    final Environment environment) {
        environment.jersey().register(new AuthValueFactoryProvider.Binder<>(User.class));
        environment.jersey().register(RolesAllowedDynamicFeature.class);
        environment.getApplicationContext().setSessionHandler(new SessionHandler());
        environment.getApplicationContext()
                .addServlet(NAME, "/ckfinder/*")
                .getRegistration()
                .setMultipartConfig(new MultipartConfigElement("/tmp", 5242880, 20971520, 0));
    }
}
