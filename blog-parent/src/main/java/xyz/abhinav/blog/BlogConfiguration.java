package xyz.abhinav.blog;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.bundles.assets.AssetsBundleConfiguration;
import io.dropwizard.bundles.assets.AssetsConfiguration;
import io.dropwizard.db.DataSourceFactory;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
public class BlogConfiguration extends Configuration implements BlogRestConfiguration, AssetsBundleConfiguration {
    @Valid
    @NotNull
    @JsonProperty("database")
    public DataSourceFactory database = new DataSourceFactory();

    @JsonProperty("swagger")
    public SwaggerBundleConfiguration swaggerBundleConfiguration;

    @Valid
    @NotNull
    @JsonProperty("mediaRoot")
    public String mediaRoot;

    @Valid
    @NotNull
    @JsonProperty("mediaURL")
    public String mediaURL;

    @Valid
    @NotNull
    @JsonProperty("assets")
    private final AssetsConfiguration assetsConfiguration = AssetsConfiguration.builder().build();


}
