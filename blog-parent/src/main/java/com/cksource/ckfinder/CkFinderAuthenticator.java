package com.cksource.ckfinder;

import com.cksource.ckfinder.authentication.Authenticator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jose4j.lang.JoseException;
import xyz.abhinav.auth.services.JwtKeyService;

import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

@Named
@AllArgsConstructor
@Slf4j
public class CkFinderAuthenticator implements Authenticator {
    private final HttpServletRequest request;
    private final JwtKeyService jwtKeyService = new JwtKeyService();

    @Override
    public boolean authenticate() {
        try {
            if(request.getParameterMap().get("command")[0].equals("Thumbnail") ||
                    jwtKeyService.verify(request.getHeader("Authorization").split(" ")[1]) > 0) {
                return true;
            }
        } catch (JoseException | NullPointerException e) {
            log.warn("Unauthorized attempt to access CkFinder");
        }
        return false;
    }
}
