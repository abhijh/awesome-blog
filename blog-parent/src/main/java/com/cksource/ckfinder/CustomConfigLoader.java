package com.cksource.ckfinder;

import com.cksource.ckfinder.config.Config;
import com.cksource.ckfinder.config.loader.ConfigLoader;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.springframework.core.io.ClassPathResource;

import javax.inject.Named;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Named
public class CustomConfigLoader implements ConfigLoader {
    @Override
    public Config loadConfig() throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        String path = System.getenv("CK_FINDER_CONFIG_PATH");
        if(path == null) {
            ClassPathResource resource = new ClassPathResource("ckfinder.yml");
            return mapper.readValue(resource.getInputStream(), Config.class);
        } else {
            Path configPath = Paths.get(path);
            return mapper.readValue(Files.newInputStream(configPath), Config.class);
        }
    }
}