import React, {useEffect} from 'react';
import {withRouter} from "react-router-dom";
import AddEditPost from "../components/add-edit-post/AddEditPost";
import {useDispatch, useSelector} from "react-redux";
import {fetchPostById, selectPostById, updatePost} from "../slices/postSice";

const EditPost = ({match}) => {
  const id = match.params.id
  const dispatch = useDispatch()
  const post = useSelector(
    (state) => selectPostById(state, id))

  const onSavePost = (postToUpdate, draft) => {
    dispatch(updatePost({...postToUpdate, draft}))
  }

  useEffect(() => {
    if(!post)
    dispatch(fetchPostById(id))
  }, [id])

  return (
    <div>
      {post && <AddEditPost onSavePost={onSavePost} postProp={post}/>}
    </div>
  );
};

export default withRouter(EditPost);
