import React from 'react';
import AddEditPost from "../components/add-edit-post/AddEditPost";
import {savePost} from "../slices/postSice";
import {useDispatch} from "react-redux";
import {withRouter} from "react-router-dom";

const AddPost = ({history}) => {
  const dispatch = useDispatch()
  const onSavePost = (post, draft) => {
    dispatch(savePost({...post, draft})).then(action => {
      if(action.type === 'adminPosts/save/fulfilled') {
        history.push(`/admin/edit-post/${action.payload.id}`)
      }
    })
  }

  return (<AddEditPost onSavePost={onSavePost}/>);
}

export default withRouter(AddPost);
