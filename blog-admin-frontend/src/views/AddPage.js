import React from 'react';
import {savePage} from "../slices/pageSice";
import {useDispatch} from "react-redux";
import AddEditPage from "../components/add-edit-page/AddEditPage";
import {withRouter} from "react-router-dom";

const AddPage = ({history}) => {
  const dispatch = useDispatch()
  const onSavePage = (page, draft) => {
    dispatch(savePage({...page, draft})).then(action => {
      if(action.type === 'adminPages/save/fulfilled') {
        history.push(`/admin/edit-page/${action.payload.data.id}`)
      }
    })
  }

  return (<AddEditPage onSavePage={onSavePage}/>);
}

export default withRouter(AddPage);
