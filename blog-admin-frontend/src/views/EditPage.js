import React, {useEffect} from 'react';
import {withRouter} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchPageById, selectPageById, updatePage} from "../slices/pageSice";
import AddEditPage from "../components/add-edit-page/AddEditPage";

const EditPage = ({match}) => {
  const id = match.params.id
  const dispatch = useDispatch()
  const post = useSelector(
    (state) => selectPageById(state, id))

  const onSavePost = (pageToUpdate, draft) => {
    dispatch(updatePage({...pageToUpdate, draft}))
  }

  useEffect(() => {
    if(!post) {
      dispatch(fetchPageById(id))
    }
  }, [id])

  return (
    <div>
      {post && <AddEditPage onSavePage={onSavePost} pageProp={post}/>}
    </div>
  );
};

export default withRouter(EditPage);
