import React, {useEffect, useState} from "react";
import {Button, ButtonGroup, Card, CardBody, CardHeader, Col, Container, Row} from "shards-react";

import PageTitle from "../components/common/PageTitle";
import EditTagModal from '../components/tags/edit-tag-modal'
import {useDispatch, useSelector} from "react-redux";
import {deleteTag, fetchTags, selectAllTags} from "../slices/tagSice";

const Tags = () => {
  const dispatch = useDispatch()
  const tags = useSelector(selectAllTags)
  const [toUpdateTag, setToUpdateTag] = useState({});
  const [open, setOpen] = useState(false);
  const onUpdate = (tag) => {
    setToUpdateTag(tag)
    onToggle()
  }
  const onToggle = () => setOpen(prevState => !prevState)

  useEffect(() => {
    dispatch(fetchTags())
  }, [])
  return (
    <Container fluid className="main-content-container px-4">
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title="Tag Management" subtitle="Manage Tags" className="text-sm-left" />
      </Row>
      <Row>
        <Col>
          <Card small className="mb-4">
            <CardHeader className="border-bottom">
              <h6 className="m-0">Tags</h6>
            </CardHeader>
            <CardBody className="p-0 pb-3">
              <table className="table mb-0">
                <thead className="bg-light">
                <tr>
                  <th scope="col" className="border-0">
                    #
                  </th>
                  <th scope="col" className="border-0">
                    Name
                  </th>
                  <th scope="col" className="border-0">
                    Theme
                  </th>
                  <th scope="col" className="border-0">
                    Actions
                  </th>
                </tr>
                </thead>
                <tbody>
                {tags.map((tag, idx) => <tr key={idx}>
                  <td>{idx+1}</td>
                  <td>{tag.name}</td>
                  <td>{tag.theme}</td>
                  <td>
                    <ButtonGroup>
                      <Button onClick={() => onUpdate(tag)} theme="warning">Update</Button>
                      <Button onClick={() => dispatch(deleteTag(tag))} theme="danger">Delete</Button>
                    </ButtonGroup>
                  </td>
                </tr>)}
                </tbody>
              </table>
            </CardBody>
          </Card>
          <EditTagModal toggle={onToggle} open={open} toUpdateTag={toUpdateTag}/>
        </Col>
      </Row>
    </Container>
  );
}

export default Tags
