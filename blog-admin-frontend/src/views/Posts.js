import React, {useEffect} from "react";
import {Button, ButtonGroup, Card, CardBody, CardHeader, Col, Container, Row} from "shards-react";

import PageTitle from "../components/common/PageTitle";
import {deletePost, fetchPosts, selectAllPosts} from "../slices/postSice";
import {useDispatch, useSelector} from "react-redux";
import {withRouter} from "react-router-dom";

const Posts = ({history}) => {
  const dispatch = useDispatch()
  const posts = useSelector(selectAllPosts)
  useEffect(() => {
    dispatch(fetchPosts())
  }, [])
  return (
    <Container fluid className="main-content-container px-4">
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title="Post Management" subtitle="Manage Blog Posts" className="text-sm-left" />
      </Row>
      <Row>
        <Col>
          <Card small className="mb-4">
            <CardHeader className="border-bottom">
              <h6 className="m-0">Blog Posts</h6>
            </CardHeader>
            <CardBody className="p-0 pb-3">
              <table className="table mb-0">
                <thead className="bg-light">
                <tr>
                  <th scope="col" className="border-0">
                    #
                  </th>
                  <th scope="col" className="border-0">
                    Title
                  </th>
                  <th scope="col" className="border-0">
                    Category
                  </th>
                  <th scope="col" className="border-0">
                    Category Theme
                  </th>
                  <th scope="col" className="border-0">
                    Actions
                  </th>
                </tr>
                </thead>
                <tbody>
                {posts.map((post, idx) => <tr key={idx}>
                  <td>{idx+1}</td>
                  <td>{post.title}</td>
                  <td>{post.category}</td>
                  <td>{post.categoryTheme}</td>
                  <td>
                    <ButtonGroup>
                      <Button onClick={() => history.push(`${process.env.PUBLIC_URL}/edit-post/${post.id}`)}>Edit</Button>
                      <Button theme="success">Publish</Button>
                      <Button theme="warning">Rollback</Button>
                      <Button onClick={() => dispatch(deletePost(post))} theme="danger">Delete</Button>
                    </ButtonGroup>
                  </td>
                </tr>)}
                </tbody>
              </table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default withRouter(Posts);
