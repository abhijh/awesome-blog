// import React, {useState} from 'react'
// import {Link, Redirect, withRouter} from "react-router-dom"
// import './login.css'
// import {useDispatch, useSelector} from "react-redux";
// import {login} from '../../slices/authSice'
//
// const Login = ({history, lastRoute}) => {
//   const dispatch = useDispatch()
//   const [email, setEmail] = useState('');
//   const [password, setPassword] = useState('');
//   const accessToken = useSelector(state => state.auth.accessToken)
//   const handleOnSubmit = (e) => {
//     e.preventDefault()
//     dispatch(login({
//       email, password
//     })).then(response => {
//       if(response.type === login.fulfilled.toString()) {
//         console.log('Logged in Successfully')
//         // history.push(lastRoute)
//       }
//     })
//   }
//   return (
//     <div className="login-wrapper">
//       {!accessToken? <div className="login-container">
//         <div className="d-flex justify-content-center h-100">
//           <div className="card login-card">
//             <div className="card-header login-card-header">
//               <h3>Sign In</h3>
//               <div className="d-flex justify-content-end social_icon">
//                 <span><i className="fab fa-facebook-square"/></span>
//                 <span><i className="fab fa-google-plus-square"/></span>
//                 <span><i className="fab fa-twitter-square"/></span>
//               </div>
//             </div>
//             <div className="card-body">
//               <form onSubmit={handleOnSubmit}>
//                 <div className="input-group form-group">
//                   <div className="input-group-prepend login-input-group-prepend">
//                     <span className="input-group-text"><i className="fas fa-user"/></span>
//                   </div>
//                   <input value={email} onChange={(e) => {setEmail(e.target.value)}} type="text" className="form-control" placeholder="username"/>
//
//                 </div>
//                 <div className="input-group form-group">
//                   <div className="input-group-prepend login-input-group-prepend">
//                     <span className="input-group-text"><i className="fas fa-key"/></span>
//                   </div>
//                   <input value={password} onChange={(e) => {setPassword(e.target.value)}} type="password" className="form-control" placeholder="password"/>
//                 </div>
//                 <div className="row align-items-center remember">
//                   <input type="checkbox"/>Remember Me
//                 </div>
//                 <div className="form-group">
//                   <input type="submit" value="Login" className="btn float-right login_btn"/>
//                 </div>
//               </form>
//             </div>
//             <div className="card-footer login-card-footer">
//               <div className="d-flex justify-content-center links">
//                 Don't have an account?<Link to="/admin/sign-up">Sign Up</Link>
//               </div>
//               <div className="d-flex justify-content-center">
//                 <Link to="/admin/forgot-password">Forgot your password?</Link>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>: <Redirect to='/admin/blog-posts'/>}
//     </div>
//   );
// };
//
// Login.propTypes = {
//
// };
//
// export default withRouter(Login);

import React, {useState} from 'react'
import {Link, Redirect, withRouter} from "react-router-dom"
import './login.css'
import {useDispatch, useSelector} from "react-redux";
import {login} from '../slices/authSice'

const Login = ({history, lastRoute}) => {
  const dispatch = useDispatch()
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const accessToken = useSelector(state => state.auth.accessToken)
  const handleOnSubmit = (e) => {
    e.preventDefault()
    dispatch(login({
      email, password
    })).then(response => {
      if(response.type === login.fulfilled.toString()) {
        console.log('Logged in Successfully')
        // history.push(lastRoute)
      }
    })
  }
  return (
    <div className="login-wrapper">
      {!accessToken? <div className="login-container">
        <div className="d-flex justify-content-center h-100">
          <div className="card login-card">
            <div className="card-header login-card-header">
              <h3>Sign In</h3>
              <div className="d-flex justify-content-end social_icon">
                <span><i className="fab fa-facebook-square"/></span>
                <span><i className="fab fa-google-plus-square"/></span>
                <span><i className="fab fa-twitter-square"/></span>
              </div>
            </div>
            <div className="card-body">
              <form onSubmit={handleOnSubmit}>
                <div className="input-group form-group">
                  <div className="input-group-prepend login-input-group-prepend">
                    <span className="input-group-text"><i className="fas fa-user"/></span>
                  </div>
                  <input value={email} onChange={(e) => {setEmail(e.target.value)}} type="text" className="form-control" placeholder="username"/>

                </div>
                <div className="input-group form-group">
                  <div className="input-group-prepend login-input-group-prepend">
                    <span className="input-group-text"><i className="fas fa-key"/></span>
                  </div>
                  <input value={password} onChange={(e) => {setPassword(e.target.value)}} type="password" className="form-control" placeholder="password"/>
                </div>
                <div className="row align-items-center remember">
                  <input type="checkbox"/>Remember Me
                </div>
                <div className="form-group">
                  <input type="submit" value="Login" className="btn float-right login_btn"/>
                </div>
              </form>
            </div>
            <div className="card-footer login-card-footer">
              <div className="d-flex justify-content-center links">
                Don't have an account?<Link to="/admin/sign-up">Sign Up</Link>
              </div>
              <div className="d-flex justify-content-center">
                <Link to="/admin/forgot-password">Forgot your password?</Link>
              </div>
            </div>
          </div>
        </div>
      </div>: <Redirect to='/blog-posts'/>}
    </div>
  );
};

Login.propTypes = {

};

export default withRouter(Login);

