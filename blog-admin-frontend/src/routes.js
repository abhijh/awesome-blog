import React from 'react'
// Layout Types
import {AdminLayout, AuthLayout} from "./layouts";
// Route Views
import BlogOverview from "./views/BlogOverview";
import UserProfileLite from "./views/UserProfileLite";
import Errors from "./views/Errors";
import ComponentsOverview from "./views/ComponentsOverview";
import Posts from "./views/Posts";
import AddPost from "./views/AddPost";
import EditPost from "./views/EditPost";
import Login from "./views/Login";
import {Redirect} from "react-router-dom";
import Pages from "./views/Pages";
import EditPage from "./views/EditPage";
import AddPage from "./views/AddPage";
import Tags from "./views/Tags";

export default [
  {
    path: "/blog-overview",
    layout: AdminLayout,
    component: BlogOverview
  },
  {
    path: "/user-profile-lite",
    layout: AdminLayout,
    component: UserProfileLite
  },
  {
    path: "/errors",
    layout: AdminLayout,
    noFooter: true,
    noNavbar: true,
    component: Errors
  },
  {
    path: "/components-overview",
    layout: AdminLayout,
    component: ComponentsOverview
  },
  {
    path: "/login",
    layout: AuthLayout,
    component: Login,
    exact: true,
    noNavbar: true,
    noFooter: true
  },
  {
    path: "/blog-posts",
    layout: AdminLayout,
    component: Posts,
  },
  {
    path: "/edit-post/:id",
    layout: AdminLayout,
    component: EditPost
  },
  {
    path: "/add-new-post",
    layout: AdminLayout,
    component: AddPost
  },
  {
    path: "/pages",
    layout: AdminLayout,
    component: Pages,
  },
  {
    path: "/edit-page/:id",
    layout: AdminLayout,
    component: EditPage
  },
  {
    path: "/add-new-page",
    layout: AdminLayout,
    component: AddPage
  },
  {
    path: "/tags",
    layout: AdminLayout,
    component: Tags
  },
  {
    path: '*',
    layout: AdminLayout,
    component: () => <Redirect to={`${process.env.PUBLIC_URL}/blog-overview`} />
  }
];
