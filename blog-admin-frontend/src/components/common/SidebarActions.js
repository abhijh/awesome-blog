/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import Moment from 'react-moment';
import 'moment-timezone';

import {Button, Card, CardBody, CardHeader, ListGroup, ListGroupItem} from "shards-react";
import Spinner from "./Spinner/Spinner";
import {useSelector} from "react-redux";

const SidebarActions = ({
                          onSavePostAsDraft,
                          onPublishPost,
                          draft,
                          createdDate,
                          updatedDate
                        }) => {
  const isLoading = useSelector(state => state.response.isLoading)
  return (
    <Card small className="mb-3">
      <CardHeader className="border-bottom">
        <h6 className="m-0">Actions</h6>
      </CardHeader>

      <CardBody className="p-0">
        <ListGroup flush>
          <ListGroupItem className="p-3">
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">flag</i>
            <strong className="mr-1">Status:</strong>
            <strong className={draft ? 'text-warning' : 'text-success'}>{draft ? 'Draft' : 'Published'}</strong>{" "}
            <a className="ml-auto" href="#">
              Edit
            </a>
          </span>
            {/*<span className="d-flex mb-2">*/}
            {/*  <i className="material-icons mr-1">visibility</i>*/}
            {/*  <strong className="mr-1">Visibility:</strong>{" "}*/}
            {/*    <strong className="text-success">Public</strong>{" "}*/}
            {/*    <a className="ml-auto" href="#">*/}
            {/*    Edit*/}
            {/*  </a>*/}
            {/*</span>*/}
            <span className="d-flex mb-2">
            <i className="material-icons mr-1">calendar_today</i>
            <strong className="mr-1">Created:</strong>
            <strong className="ml-auto">
              <Moment format="DD/MM/YY hh:mm:ss" date={createdDate}/>
            </strong>
          </span>
            <span className="d-flex">
            <i className="material-icons mr-1">score</i>
            <strong className="mr-1">Updated:</strong>
            <strong className="ml-auto">
              <Moment format="DD/MM/YY hh:mm:ss" date={updatedDate}/>
            </strong>
          </span>
          </ListGroupItem>
          <ListGroupItem className="d-flex px-3 border-0">
            <Button
              style={{
                width: '100px',
                height: '32px'
              }}
              outline theme="accent"
              size="sm"
              onClick={(e) => !isLoading && onSavePostAsDraft(e)}>
              {isLoading ?
                <Spinner/> :
                <span><i className="material-icons">save</i> Save Draft</span>}
            </Button>
            <Button
              style={{
                width: '90px',
                height: '32px'
              }}
              theme="accent"
              size="sm"
              className="ml-auto"
              onClick={(e) => !isLoading && onPublishPost(e)}>
              {isLoading ?
                <Spinner/> :
                <span><i className="material-icons">file_copy</i> Publish</span>}
            </Button>
          </ListGroupItem>
        </ListGroup>
      </CardBody>
    </Card>
  );
}

export default SidebarActions;
