import React from "react";
import MdEditor  from 'react-markdown-editor-lite'
import ReactMarkdown from "react-markdown/with-html";
import {Image, CodeBlock} from "../renderers";
import Ckfinder from "./plugins/Ckfinder";
import "./editor.css";
import 'react-markdown-editor-lite/lib/index.css'

MdEditor.use(Ckfinder)
const Editor = ({value, setData, height}) => {
  const handleEditorChange = ({text}) => {
    setData({'body': text})
  }

  const handleRenderHTML = (text) => {
    return (
      <ReactMarkdown
        source={text}
        escapeHtml={false}
        renderers={{
          code: CodeBlock,
          image: Image
        }}/>
    )
  }

  return (
    <MdEditor
      style={{...styles.mdEditor, height: height? height: '533px'}}
      value={value}
      renderHTML={handleRenderHTML}
      onChange={handleEditorChange}
    />);
}

const styles = {
  mdEditor: {
    zIndex: 1070
  }
}

export default Editor;
