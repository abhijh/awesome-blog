import React from 'react'
import {PluginComponent} from 'react-markdown-editor-lite';

export default class Ckfinder extends PluginComponent {
    // Define plugin name here, must be unique
    static pluginName = 'ckfinder';
    // Define which place to be render, default is left, you can aslo use 'right'
    static align = 'left';
    // Define default config if required
    static defaultConfig = {
        start: 0
    }

    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);

        this.state = {
            num: this.getConfig('start')
        };
    }

    // handleClick() {
    //     // Call API, insert number to editor
    //     this.editor.insertText(this.state.num);
    //     // Update itself's state
    //     this.setState(previousState => ({
    //         num: ++previousState.num
    //     }))
    // }
    handleClick(e) {
        const editor = this.editor
        e.preventDefault()
        window.CKFinder.modal({
            connectorPath: "/ckfinder/connector",
            chooseFiles: true,
            width: 800,
            height: 600,
            onInit: function( finder ) {
                finder.on( 'files:choose', function( evt ) {
                    const file = evt.data.files.first();
                    editor.insertMarkdown('image', {
                        target: file.attributes.name,
                        imageUrl: file.getUrl(),
                    });
                    // setPostData({'backgroundImage': file.getUrl()})
                    // setBackgroundImageUrl(file.getUrl())
                });

                finder.on( 'file:choose:resizedImage', function( evt ) {
                    // setPostData({'backgroundImage': evt.data.resizedUrl})
                    // setBackgroundImageUrl(evt.data.resizedUrl)
                });
            }
        });
    }

    render() {
        return (
            <span
                className="button"
                title="Counter"
                onClick={this.handleClick}>
                <i className="rmel-iconfont rmel-icon-image"/>
            </span>
        );
    }
}