import React from "react";
import {Alert, Container, Fade} from "shards-react";
import {connect} from "react-redux";

class AdminAlert extends React.Component {
  constructor(props) {
    super(props);

    this.interval = null;
    this.state = {
      visible: false,
      countdown: 0,
      timeUntilDismissed: 5
    };

    this.showAlert = this.showAlert.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.clearInterval = this.clearInterval.bind(this);
    this.dismiss = this.dismiss.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevProps.isLoading === true &&
      this.props.isLoading === false) {
      this.showAlert()
    }
  }

  showAlert() {
    this.clearInterval();
    this.setState({visible: true, countdown: 0, timeUntilDismissed: 5});
    this.interval = setInterval(this.handleTimeChange, 1000);
  }

  handleTimeChange() {
    if (this.state.countdown < this.state.timeUntilDismissed - 1) {
      this.setState({
        ...this.state,
        ...{countdown: this.state.countdown + 1}
      });
    } else {
      this.dismiss()
    }
  }

  dismiss() {
    this.setState({...this.state, ...{visible: false}});
    this.clearInterval();
  }

  clearInterval() {
    clearInterval(this.interval);
    this.interval = null;
  }

  render() {
    return (
      <Container fluid className="px-0">
        <Fade style={{
          height: '43px'
        }} timeout={150} in={this.props.message !== undefined &&
        this.state.visible}>
          <Alert
            className="mb-0"
            dismissible={this.dismiss}
            theme={`${this.props.theme? this.props.theme: 'success'}`}>
            {this.props.message}
          </Alert>
        </Fade>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.response.isLoading,
    message: state.response.message,
    theme: state.response.theme
  }
}

export default connect(mapStateToProps)(AdminAlert)