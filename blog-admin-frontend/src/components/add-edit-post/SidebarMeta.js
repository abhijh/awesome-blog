import React from "react";
import PropTypes from "prop-types";
import {Card, CardBody, CardHeader, ListGroup} from "shards-react";
import Category from "./Category";
import SelectTags from "./SelectTags";

const SidebarMeta = ({tags, category, categoryTheme, setPostData}) => {
  return (
    <Card small className="mb-3">
      <CardHeader className="border-bottom">
        <h6 className="m-0">Meta</h6>
      </CardHeader>
      <CardBody className="p-0">
        <ListGroup flush
          style={{
            padding: '10px'
          }}>
          <Category
            categoryTheme={categoryTheme}
            category={category}
            setPostData={setPostData}/>
        </ListGroup>
        <ListGroup flush
          style={{
            padding: '10px'
          }}>
          <SelectTags
            setPostData={setPostData}
            tags={tags}/>
        </ListGroup>
      </CardBody>
    </Card>
  );
}

SidebarMeta.propTypes = {
  setPostData: PropTypes.func.isRequired
};

export default SidebarMeta;
