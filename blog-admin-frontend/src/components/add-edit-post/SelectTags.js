import React, {useEffect} from 'react'
import PropTypes from 'prop-types'
import Creatable from "react-select/creatable"
import makeAnimated from 'react-select/animated'
import {useDispatch, useSelector} from "react-redux"
import {selectAllTags, fetchTags, saveTag} from "../../slices/tagSice"

const animatedComponents = makeAnimated()
const SelectTags = ({tags, setPostData}) => {
  const dispatch = useDispatch()
  const options = useSelector(selectAllTags)
  const isLoading = useSelector(state => state.response.isLoading)
  useEffect(() => {dispatch(fetchTags())}, [])

  const onChange = (tags) => {
    setPostData({tags: tags})
  }

  const onCreate = (inputValue) => {
    dispatch(saveTag({name: inputValue, theme: 'success'}))
      .then(response => {
        console.log(response)
        if(response.type === saveTag.fulfilled.toString()) {
          setPostData({tags: [...tags, response.payload.data]})
        }
      })
  }

  return (
    <Creatable
      isClearable
      isMulti
      isDisabled={isLoading}
      isLoading={isLoading}
      components={animatedComponents}
      options={options}
      className="basic-multi-select"
      classNamePrefix="select"
      onChange={onChange}
      onCreateOption={onCreate}
      getOptionLabel={(option)=>option.name}
      getOptionValue={(option)=>option.id}
      getNewOptionData={(inputValue, optionLabel) => ({
          id: inputValue,
          name: optionLabel,
          __isNew__: true
        })}
      value={tags}
    />
  );
};

SelectTags.propTypes = {
  tags: PropTypes.arrayOf(Object),
  setPostData: PropTypes.func.isRequired
};

SelectTags.defaultProps = {
  tags: []
}

export default SelectTags;
