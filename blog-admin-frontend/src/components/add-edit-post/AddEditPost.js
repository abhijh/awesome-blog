import React, {useState} from "react"
import PropTypes from "prop-types";

import {Card, CardBody, Col, Container, FormInput, FormTextarea, Row} from "shards-react"
import PageTitle from "../common/PageTitle"
import Editor from "../common/editor/Editor"
import SidebarActions from "../common/SidebarActions"
import SidebarMeta from "./SidebarMeta"
import SidebarImage from "./SidebarImage";

const AddEditPost = ({postProp, onSavePost}) => {
  const [post, setPost] = useState(postProp)

  const handleOnChange = (e) => {
    const datum = Object
    datum[e.target.name] = e.target.value
    setPostData(datum)
    e.persist()
  }

  const setPostData = (data) => {
    setPost((prevState) => {
      return {
        ...prevState,
        ...data
      }
    })
  }

  return (
    <Container fluid className="main-content-container px-4 pb-4">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title={postProp.title} subtitle="Edit Post" className="text-sm-left" />
      </Row>
      <Row>
        {/* Editor */}
        <Col lg="9" md="12">
          <div className="add-new-post">
            <Card small className="mb-3">
              <CardBody>
                <FormInput
                  onChange={handleOnChange}
                  value={post.title}
                  name="title"
                  size="lg"
                  className="mb-3"
                  placeholder="Post Title"/>
                <FormTextarea
                  onChange={handleOnChange}
                  value={post.description}
                  name="description"
                  size="lg"
                  className="mb-3"
                  placeholder="Post Description"/>
                <Editor value={post.body} setData={setPostData}/>
              </CardBody>
            </Card>
          </div>
        </Col>
        {/* Sidebar Widgets */}
        <Col lg="3" md="12">
          <SidebarActions
            {...postProp}
            onSavePostAsDraft={() => onSavePost(post, true)}
            onPublishPost={() => onSavePost(post, false)}/>
          <SidebarMeta {...post} setPostData={setPostData}/>
          <SidebarImage backgroundImageUrlProp={postProp.backgroundImage} setPostData={setPostData}/>
        </Col>
      </Row>
    </Container>
  );
}

AddEditPost.propTypes = {
  postProp: PropTypes.object.isRequired
};

AddEditPost.defaultProps = {
  postProp: {
    draft: true,
    title: '',
    body: '',
    category: '',
    categoryTheme: '',
    backgroundImage: ''
  }
};

export default AddEditPost;
