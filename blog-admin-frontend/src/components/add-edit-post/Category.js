import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

const categories = [
  {
    category: 'Technology',
    categoryTheme: 'primary'
  },
  {
    category: 'Travel',
    categoryTheme: 'danger'
  },
  {
    category: 'Food',
    categoryTheme: 'success'
  }
];

const Category = ({category, categoryTheme, setPostData}) => {
  const selectedCategory = {
    category: category,
    categoryTheme: categoryTheme
  }
  const handleSelection = (selectedCategory) => setPostData({...selectedCategory})

  return (
      <Select
        style={{
          margin: '10px',
          padding: '10px'
        }}
        value={selectedCategory}
        onChange={handleSelection}
        getOptionLabel={(option)=>option.category}
        getOptionValue={(option)=>option.categoryTheme}
        options={categories}/>
  );
};

Category.propTypes = {
  setPostData: PropTypes.func.isRequired
};

export default Category;
