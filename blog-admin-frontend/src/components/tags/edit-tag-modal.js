import React, {useEffect, useState} from 'react'
import {Button, FormInput, Modal, ModalBody, ModalFooter, ModalHeader} from "shards-react";
import {useDispatch} from "react-redux";
import {updateTag} from "../../slices/tagSice";

const EditTagModal = ({open, toggle, toUpdateTag}) => {
  const [tag, setTag] = useState({})
  const dispatch = useDispatch()
  useEffect(() => {
    setTag(toUpdateTag)
  }, [toUpdateTag])
  const onChange = (e) => {
    const datum = {}
    datum[e.target.name] = e.target.value
    setTag((prevState) => {
      return {
        ...prevState,
        ...datum
      }
    })
    e.persist()
  }

  const onSave = () => {
    dispatch(updateTag(tag))
      .then(response => {
        if(response.type === updateTag.fulfilled.toString()) {
          toggle()
        }
      })
  }

  return (
    <Modal open={open}>
      <ModalHeader>Editing {toUpdateTag.name}</ModalHeader>
      <ModalBody>
        <FormInput
          onChange={onChange}
          value={tag.name}
          size="sm"
          placeholder="Name"
          name="name"
          className="mb-2" />
        <FormInput
          onChange={onChange}
          value={tag.theme}
          size="sm"
          placeholder="Theme"
          name="theme"
          className="mb-2" />
      </ModalBody>
      <ModalFooter>
        <Button onClick={onSave} theme="success">Save</Button>
        <Button onClick={() => toggle()} theme="danger">Cancel</Button>
      </ModalFooter>
    </Modal>
  );
}

export default EditTagModal