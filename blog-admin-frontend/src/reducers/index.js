import {combineReducers} from 'redux';
import postsReducer from '../slices/postSice'
import tagsReducer from '../slices/tagSice'
import pagesReducer from '../slices/pageSice'
import authReducer from '../slices/authSice'
import responseReducer from '../slices/responseSlice'

export default combineReducers({
  response: responseReducer,
  posts: postsReducer,
  tags: tagsReducer,
  pages: pagesReducer,
  auth: authReducer
})
