import LocalStorageService from "./LocalStorageService";

(function(open) {
  const localStorageService = LocalStorageService.getService();
  XMLHttpRequest.prototype.open = function(method, url, async, user, pass) {
    open.call(this, method, url, async, user, pass);
    const token = localStorageService.getAccessToken()
    if(token != null) {
      this.setRequestHeader("Authorization", `Bearer ${token}`)
    }
  };
})(XMLHttpRequest.prototype.open);
