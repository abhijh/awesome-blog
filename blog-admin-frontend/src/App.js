import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import routes from "./routes";
import "bootstrap/dist/css/bootstrap.min.css";
import "./shards-dashboard/styles/shards-dashboards.1.1.0.min.css";
import './utils/interceptors'
// import withTracker from "./withTracker";

export default () => (
  <Router basename={process.env.REACT_APP_BASENAME || ""}>
    <Switch>
      {routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={`${process.env.PUBLIC_URL}${route.path}`}
            exact={route.exact}
            component={props => {
              return (
                <route.layout
                  {...props}
                  noNavbar={route.noNavbar}
                  noFooter={route.noFooter}>
                  <route.component {...props} />
                </route.layout>
              );
            }}
          />
        );
      })}
    </Switch>
  </Router>
);
