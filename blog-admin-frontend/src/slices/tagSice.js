import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit'
import {BlogsAPI} from '../apis'

const api = new BlogsAPI()

export const fetchTags = createAsyncThunk(
  'tags/fetchAll',
  async () => (await api.fetchAllTags()).data
)

export const saveTag = createAsyncThunk(
  'tags/save',
  async (tag, _) => {
    try {
      const response = (await api.saveTag(tag))
      return response.data
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

export const deleteTag = createAsyncThunk(
  'tags/delete',
  async (tag, _) => {
    try {
      const data = (await api.deleteTag(tag.id)).data
      return {...data, id: tag.id}
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

export const updateTag = createAsyncThunk(
  'tags/update',
  async (tag, _) => {
    try {
      const response = (await api.updateTag(tag))
      return response.data
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

const tagsAdapter = createEntityAdapter()

const initialState = tagsAdapter.getInitialState()

const tagsSlice = createSlice({
  name: 'tags',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchTags.fulfilled, (state, action) => {
      tagsAdapter.setAll(state, action.payload.data)
    })
    builder.addCase(saveTag.fulfilled, ((state, action) => {
      tagsAdapter.upsertOne(state, action.payload.data)
    }))
    builder.addCase(updateTag.fulfilled, ((state, action) => {
      tagsAdapter.upsertOne(state, action.payload.data)
    }))
    builder.addCase(deleteTag.fulfilled, ((state, action) => {
      tagsAdapter.removeOne(state, action.payload.id)
    }))
  }
})

export const {
  selectById: selectTagById,
  selectIds: selectTagIds,
  selectEntities: selectTagEntities,
  selectAll: selectAllTags,
  selectTotal: selectTotalTags
} = tagsAdapter.getSelectors(state => state.tags)

export default tagsSlice.reducer
