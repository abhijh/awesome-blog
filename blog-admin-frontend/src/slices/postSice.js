import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit'
import {BlogsAPI} from '../apis'

const api = new BlogsAPI()

export const fetchPosts = createAsyncThunk(
  'posts/fetchAll',
  async () => (await api.fetchAllPosts()).data
)

export const fetchPostById = createAsyncThunk(
  'posts/fetchById',
  async (id, _) => {
    try {
      const response = await api.fetchPostById(id)
      return response.data
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

export const savePost = createAsyncThunk(
  'posts/save',
  async (post, _) => {
    try {
      const response = (await api.savePost(post))
      return response.data
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

export const deletePost = createAsyncThunk(
  'posts/delete',
  async (post, _) => {
    try {
      const data = (await api.deletePost(post.id)).data
      return {...data, id: post.id}
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

export const updatePost = createAsyncThunk(
  'posts/update',
  async (post, _) => {
    try {
      const response = (await api.updatePost(post))
      return response.data
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

const postsAdapter = createEntityAdapter()
const initialState = postsAdapter.getInitialState()

const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchPosts.fulfilled, (state, action) => {
      postsAdapter.setAll(state, action.payload.data)
    })
    builder.addCase(savePost.fulfilled, ((state, action) => {
      postsAdapter.upsertOne(state, action.payload.data)
    }))
    builder.addCase(updatePost.fulfilled, ((state, action) => {
      postsAdapter.upsertOne(state, action.payload.data)
    }))
    builder.addCase(fetchPostById.fulfilled, ((state, action) => {
      postsAdapter.upsertOne(state, action.payload.data)
    }))
    builder.addCase(deletePost.fulfilled, ((state, action) => {
      postsAdapter.removeOne(state, action.payload.id)
    }))
  }
})

export const {
  selectById: selectPostById,
  selectIds: selectPostIds,
  selectEntities: selectPostEntities,
  selectAll: selectAllPosts,
  selectTotal: selectTotalPosts
} = postsAdapter.getSelectors(state => state.posts)

export default postsSlice.reducer
