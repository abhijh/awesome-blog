import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {BlogsAPI} from '../apis'
import LocalStorageService from "../utils/LocalStorageService";

const api = new BlogsAPI()
const localStorageService = LocalStorageService.getService();

export const login = createAsyncThunk(
  'auth/login',
  async (payload) => (await api.login(payload)).data
)

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isLoading: false,
    accessToken: localStorageService.getAccessToken()
  },
  reducers: {},
  extraReducers: builder => {
    builder.addCase(login.pending, state => {
      state.isLoading = true
    })
    builder.addCase(login.fulfilled, (state, action) => {
      state.isLoading = false
      state.accessToken = action.payload.key
      localStorageService.setAccessToken(action.payload.key)
    })
    builder.addCase(login.rejected, (state, action) => {
      console.log(action.error)
      state.isLoading = false
    })
  }
})

export default authSlice.reducer
