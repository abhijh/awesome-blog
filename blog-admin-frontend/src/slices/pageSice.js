import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit'
import {BlogsAPI} from '../apis'


const api = new BlogsAPI()

export const fetchPages = createAsyncThunk(
  'pages/fetchAll',
  async () => (await api.fetchAllPages()).data
)

export const fetchPageById = createAsyncThunk(
  'pages/fetchId',
  async (slug, _) => {
    try {
      const response = await api.fetchPageById(slug)
      return response.data
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

export const savePage = createAsyncThunk(
  'pages/save',
  async (page, _) => {
    try {
      const response = await api.savePage(page)
      return response.data
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

export const deletePage = createAsyncThunk(
  'pages/delete',
  async (page, _) => {
    try {
      const data = (await api.deletePage(page.id)).data
      return {...data, id: page.id}
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

export const updatePage = createAsyncThunk(
  'pages/update',
  async (page, _) => {
    try {
      const response = (await api.updatePage(page))
      return response.data
    } catch (err) {
      if (!err.response) {
        throw err
      }
      return _.rejectWithValue(err.response.data)
    }
  }
)

const pagesAdapter = createEntityAdapter()
const initialState = pagesAdapter.getInitialState()

export const pagesSlice = createSlice({
  name: 'pages',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchPages.fulfilled, (state, action) => {
      pagesAdapter.setAll(state, action.payload.data)
    })
    builder.addCase(fetchPageById.fulfilled, ((state, action) => {
      pagesAdapter.upsertOne(state, action.payload.data)
    }))
    builder.addCase(savePage.fulfilled, ((state, action) => {
      pagesAdapter.upsertOne(state, action.payload.data)
    }))
    builder.addCase(updatePage.fulfilled, ((state, action) => {
      pagesAdapter.upsertOne(state, action.payload.data)
    }))
    builder.addCase(deletePage.fulfilled, ((state, action) => {
      pagesAdapter.removeOne(state, action.payload.id)
    }))
  }
})

export const {
  selectById: selectPageById,
  selectIds: selectPageIds,
  selectEntities: selectPageEntities,
  selectAll: selectAllPages,
  selectTotal: selectTotalPages
} = pagesAdapter.getSelectors(state => state.pages)

export default pagesSlice.reducer
