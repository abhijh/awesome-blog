import {createSlice} from "@reduxjs/toolkit";
import {deletePage, fetchPageById, fetchPages, savePage, updatePage} from "./pageSice";
import {deletePost, fetchPostById, fetchPosts, savePost, updatePost} from "./postSice";
import {deleteTag, fetchTags, saveTag} from "./tagSice";

const initialState = {
  message: '',
  theme: '',
  isLoading: false
}

const responseSlice = createSlice({
  name: 'response',
  initialState,
  extraReducers: builder => {
    builder.addCase(fetchPostById.pending, (state, action) => 
      handlePending(state))
    builder.addCase(fetchPosts.pending, (state, action) => 
      handlePending(state))
    builder.addCase(updatePost.pending, (state, action) => 
      handlePending(state))
    builder.addCase(savePost.pending, (state, action) =>
      handlePending(state))
    builder.addCase(deletePost.pending, (state, action) =>
      handlePending(state))
    builder.addCase(fetchPageById.pending, (state, action) => 
      handlePending(state))
    builder.addCase(fetchPages.pending, (state, action) => 
      handlePending(state))
    builder.addCase(updatePage.pending, (state, action) => 
      handlePending(state))
    builder.addCase(savePage.pending, (state, action) => 
      handlePending(state))
    builder.addCase(deletePage.pending, (state, action) =>
      handlePending(state))
    builder.addCase(saveTag.pending, (state, action) =>
      handlePending(state))
    builder.addCase(fetchTags.pending, (state, action) =>
      handlePending(state))

    builder.addCase(fetchPostById.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(fetchPosts.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(updatePost.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(savePost.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(deletePost.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(fetchPageById.rejected, (state, action) => 
      handleRejected(state, action))
    builder.addCase(fetchPages.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(updatePage.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(savePage.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(deletePage.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(fetchTags.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(deleteTag.rejected, (state, action) =>
      handleRejected(state, action))
    builder.addCase(saveTag.rejected, (state, action) =>
      handleRejected(state, action))

    builder.addCase(fetchPostById.fulfilled, (state, action) => 
      handleFulfilled(state, action))
    builder.addCase(updatePost.fulfilled, (state, action) => 
      handleFulfilled(state, action))
    builder.addCase(savePost.fulfilled, (state, action) => 
      handleFulfilled(state, action))
    builder.addCase(deletePost.fulfilled, (state, action) =>
      handleFulfilled(state, action))
    builder.addCase(fetchPageById.fulfilled, (state, action) =>
      handleFulfilled(state, action))
    builder.addCase(fetchPages.fulfilled, (state, action) => 
      handleFulfilled(state, action))
    builder.addCase(updatePage.fulfilled, (state, action) => 
      handleFulfilled(state, action))
    builder.addCase(savePage.fulfilled, (state, action) => 
      handleFulfilled(state, action))
    builder.addCase(deletePage.fulfilled, (state, action) =>
      handleFulfilled(state, action))
    builder.addCase(fetchTags.fulfilled, (state, action) =>
      handleFulfilled(state, action))
    builder.addCase(deleteTag.fulfilled, (state, action) =>
      handleFulfilled(state, action))
    builder.addCase(saveTag.fulfilled, (state, action) =>
      handleFulfilled(state, action))
  }
})

const handleFulfilled = (state, action) => {
  state.message = action.payload.message
  state.theme = 'success'
  state.isLoading = false;
}

const handleRejected = (state, action) => {
  state.message = action.payload? action.payload.message: action.error.message
  state.theme = 'danger'
  state.isLoading = false
}

const handlePending = (state) => {
  state.message = ''
  state.theme = 'dark'
  state.isLoading = true
}

export default responseSlice.reducer
