import React from "react";
import PropTypes from "prop-types";
import {Redirect} from "react-router-dom";

import {Col, Container, Row} from "shards-react";
import AdminNavbar from "../components/layout/Navbar/AdminNavbar";
import MainSidebar from "../components/layout/MainSidebar/MainSidebar";
import MainFooter from "../components/layout/MainFooter";
import LocalStorageService from "../utils/LocalStorageService";
import AdminAlert from "../components/layout/AdminAlert";

const localStorageService = LocalStorageService.getService()

const AdminLayout = ({ children, noNavbar, noFooter }) => {
  return (
    <Container fluid>
      {localStorageService.getAccessToken()?
      <Row>
        <MainSidebar />
        <Col
          className="main-content p-0"
          lg={{ size: 10, offset: 2 }}
          md={{ size: 9, offset: 3 }}
          sm="12"
          tag="main"
        >
          {!noNavbar && <AdminNavbar />}
          <AdminAlert/>
          {children}
          {!noFooter && <MainFooter />}
        </Col>
      </Row>: <Redirect to={`${process.env.PUBLIC_URL}/login`}/>}
    </Container>
  );
}

AdminLayout.propTypes = {
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool,
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool
};

AdminLayout.defaultProps = {
  noNavbar: false,
  noFooter: false
};

export default AdminLayout;
